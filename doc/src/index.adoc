== JMdictDB documentation

* link:install.html[Installation Guide]
* link:oper.html[Operations Guide]
* link:dev.html[Development Guide]
* link:tags.html[Modifying JMdictDB tags]

* The JMdictDB database schema:
** link:schema.pdf[Textual description (PDF)]
** link:schema.png[Schema diagram (PNG)]

* Updates:

** link:2023-08-update.html[2023-08-update] --
   This update removes the branched-edits "feature" of JMdictDB and
   enforces a linear chain of edits on all entries.  This document
   describes how editing has changed and how to perform the update.
** link:2021-11-update.html[2021-11-update] --
   This update provides instructions for switching from the CGI backend
   to WSGI/Flask.
** link:2020-06-changes.html[2020-06-changes] --
   Summary of the significant changes in the June 2020 update
** link:2020-06-update.html[2020-06-update.html] --
   Instructions for applying the June 2020 update

These files are located in the JMdictDB project doc/ directory.
