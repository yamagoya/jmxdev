<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="Asciidoctor 2.0.16">
<title>2023-08 Update For No-branch Edits</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic%7CNoto+Serif:400,400italic,700,700italic%7CDroid+Sans+Mono:400,700">
<link rel="stylesheet" href="./asciidoctor.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="article">
<div id="header">
<h1>2023-08 Update For No-branch Edits</h1>
<div id="toc" class="toc">
<div id="toctitle">Table of Contents</div>
<ul class="sectlevel1">
<li><a href="#_user_visible_changes">1. User-visible changes</a>
<ul class="sectlevel2">
<li><a href="#_edits">1.1. Edits</a></li>
<li><a href="#_concurrent_edits">1.2. Concurrent edits</a></li>
<li><a href="#_rejects">1.3. Rejects</a></li>
<li><a href="#_deletes">1.4. Deletes</a></li>
<li><a href="#_search_results">1.5. Search results</a></li>
<li><a href="#_other_changes">1.6. Other changes</a></li>
</ul>
</li>
<li><a href="#upd-apply">2. Applying the update</a>
<ul class="sectlevel2">
<li><a href="#redirects">2.1. Redirect CGI URLs</a></li>
<li><a href="#_code_and_database_update">2.2. Code and database update</a></li>
</ul>
</li>
<li><a href="#_acknowledgments">3. Acknowledgments</a></li>
</ul>
</div>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>This update removes the branched-edits "feature" of JMdictDB and enforces
a linear sequence of edit versions on all entries.</p>
</div>
<div class="admonitionblock important">
<table>
<tr>
<td class="icon">
<i class="fa icon-important" title="Important"></i>
</td>
<td class="content">
This revision will break CGI access to JMdictDB; the
changes to the WSGI view code are too extensive to backport to CGI with
the available resources.  Please read section <a href="#upd-apply"> 2, &#8220;Applying the update&#8221;</a> before
making any changes.
</td>
</tr>
</table>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_user_visible_changes">1. User-visible changes</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="_edits">1.1. Edits</h3>
<div class="admonitionblock note">
<table>
<tr>
<td class="icon">
<i class="fa icon-note" title="Note"></i>
</td>
<td class="content">
In the ascii-art diagrams below, entry versions are represented
by A, B, C, etc and the state of the entry in parenthesis:
A=active, D=deleted, R=rejected, *=unapproved, no astrisk means approved.
The arrows denote sequences of edits with oldest on left, newest on right.
</td>
</tr>
</table>
</div>
<div class="sect3">
<h4 id="_the_old_way">1.1.1. The old way</h4>
<div class="paragraph">
<p>As the JMdictDB system was originally designed, any entry version can be
edited.  When two edits were made to the same parent entry, an edit branch
(a.k.a fork) was created.  Given an approved entry, A, with two subsequent
two edits, B and C:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) ---&gt; B(A*) ---&gt; C(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>One could edit A again resulting in:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) -.----&gt; B(A*) ---&gt; C(A*)
       \
        `--&gt; D(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>Before entry version D could be approved, entry version C would have
to be rejected (or visa versa).</p>
</div>
</div>
<div class="sect3">
<h4 id="_the_new_way">1.1.2. The new way</h4>
<div class="paragraph">
<p>This update enforces a linear order to edits.</p>
</div>
<div class="paragraph">
<p>One may still edit A a second time as shown above but now when the
entry is submitted, D (the second edit of A) will be
applied to C, not to A.  The result will be:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) ---&gt; B(A*) ---&gt; C(A*) ---&gt; D(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>Since the content of D is based on A (plus any additional changes
made in the edit) the changes made in B and C will in effect be
reverted.  The history records for D will include those from D,C,B,A;
the diff in history record D will be for the changes D made relative
to C, not A.  The end result is if C had been edited but the content
pasted into the Edit form was from A (plus any additional
changes the edit was intended to provide.)</p>
</div>
<div class="paragraph">
<p>To prevent this from happening unknowingly, after A is edited the
second time and the Next button clicked, the Edit Confirmation
page will show a warning that says,</p>
</div>
<div class="exampleblock">
<div class="content">
Warning: Other edits have been made to this entry. If you submit your edit,
your changes will supersede those edits. Please review the history items for
those edits in red below and only submit if you are sure those changes are
already incorporated in your edit or you wish to intentionally exclude them.
</div>
</div>
<div class="paragraph">
<p>And in the entry&#8217;s History section, the records for the edits 2 and 3
(represented by B and C above) are shown in red:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>History:
.  A* 2023-08-18 05:28:59  edit4
   Diff:
         @@ -12 +12 @@
         -&lt;gloss&gt;edit2&lt;/gloss&gt;
         +&lt;gloss&gt;instruction&lt;/gloss&gt;
3. A* 2023-07-07 13:36:04  edit2   &lt;------ red
   Diff:
         @@ -12 +12 @@
         -&lt;gloss&gt;edit1&lt;/gloss&gt;
         +&lt;gloss&gt;edit2&lt;/gloss&gt;
2. A* 2023-07-07 13:35:45  edit1   &lt;------ red
   Diff:
         @@ -12 +12 @@
         -&lt;gloss&gt;instruction&lt;/gloss&gt;
         +&lt;gloss&gt;edit1&lt;/gloss&gt;
 1. A  2012-05-11 09:56</pre>
</div>
</div>
<div class="paragraph">
<p>You have three choices here:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>If you feel the changes that will be overwritten shown in the
history records should be overwritten you can go ahead and submit
the entry, or</p>
</li>
<li>
<p>Abandon your current edit and edit C instead, or</p>
</li>
<li>
<p>Go back to the Edit page and incorporate any changes from C you wish
to include in your edit and continue with the submission.</p>
</li>
</ul>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_concurrent_edits">1.2. Concurrent edits</h3>
<div class="paragraph">
<p>If the alternate edit(s) exist in the database when you go to the Edit
Confirmation page, the warning described above will be shown.  But it
is possible that someone submits an alternate edit after you have reached
the Edit Confirmation page (thus no warning since the alternate edit
didn&#8217;t exist yet) but before you click the Submit button.
In this case your submission attempt will produce an error rather than
being applied to that other edit.
The rationale is that you can make an informed decision about how to
proceed if you have seen the warning but not if you haven&#8217;t.</p>
</div>
</div>
<div class="sect2">
<h3 id="_rejects">1.3. Rejects</h3>
<div class="paragraph">
<p>The way rejects work now is significantly different.</p>
</div>
<div class="sect3">
<h4 id="_the_old_way_2">1.3.1. The old way</h4>
<div class="paragraph">
<p>Previously, to reject an edit branch one would reject the head (most
recent) entry on that branch:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) --.----&gt; B(A*) ---&gt; C(A*)
        \
         `--&gt; D(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>Rejecting C would implicitly reject B as well (all versions back to
the branch point) leaving:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) ---&gt; D(A*)
C(R)</pre>
</div>
</div>
</div>
<div class="sect3">
<h4 id="_the_new_way_2">1.3.2. The new way</h4>
<div class="paragraph">
<p>Now however, with linear edit chains:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) ---&gt; B(A*) ---&gt; C(A*) ---&gt; D(A*) ---&gt; E(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>Any entry version can be rejected and the rejection will encompass
all entry versions between the rejected entry and the head (most
recent) entry, and leaving the rest of the older entries alone.
Thus, rejecting version D will result in:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) ---&gt; B(A*) ---&gt; C(A*)
E(R)</pre>
</div>
</div>
<div class="paragraph">
<p>where E(R) is a rejected entry version and has history records
from E,D,C,B,A.  C can now be further edited, approved, etc as
though D and E had not existed.</p>
</div>
<div class="paragraph">
<p>Note that approved entries can not be rejected.</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A) ---&gt; B(A*) ---&gt; C(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>Trying to reject A will produce an error.  Rejecting B (and C
implicitly) will result in:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A)
C(R)</pre>
</div>
</div>
<div class="paragraph">
<p>However in:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>A(A*) ---&gt; B(A*) ---&gt; C(A*)</pre>
</div>
</div>
<div class="paragraph">
<p>where A is unapproved, A can be rejected (including B and C implicitly)
resulting in a single rejected entry:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>C(R)</pre>
</div>
</div>
<div class="paragraph">
<p>As with edits, the Edit Confirmation page will show the history
records of the edits that will be rejected in red.</p>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_deletes">1.4. Deletes</h3>
<div class="paragraph">
<p>Whereas reject applies to individual edit versions, delete applies
to the entire entry and all its versions: it is a statement that the
entry itself should not be present in the dictionary.  Like any other
edit, any entry version that is submitted for delete will be applied
to the most recent edit and when approved, will become a Deleted entry.</p>
</div>
<div class="paragraph">
<p>Like other edits, if you delete a non-head entry version, a warning
will be shown on the Edit Confirmation page and the history notes
of entries between the one edited for deletion and the head entry
will be shown in red.  This is in case the requestor was unaware there
were additional edits made, and may wish to reconsider in light of them.</p>
</div>
</div>
<div class="sect2">
<h3 id="_search_results">1.5. Search results</h3>
<div class="paragraph">
<p>The Search Results page now shows edited/unapproved entry versions
with a status letter in red.  Additionally, only the head (most recently
edited) entry version is shown with an accompanying asterisk; this is
normally the entry version that should be further edited or approved.
Non-head edited entries are shown with a "-" instead.</p>
</div>
</div>
<div class="sect2">
<h3 id="_other_changes">1.6. Other changes</h3>
<div class="paragraph">
<p>There are a number of internal changes since the last "edrdg"-tagged
revision that can be examined in more detail in the Git log.  Some of
the more notable ones are:</p>
</div>
<div class="dlist">
<dl>
<dt class="hdlist1">cgi</dt>
<dd>
<p>As mentioned above, CGI access to JMdictDB is not longer supported
and will no longer work after this update.  This was previously
forecasted to occur (see
<a href="2021-11-update.html">November 2021 WSGI Upgrade</a>)
when changes to the view code became too extensive to backport to the
CGI scripts as is the case now.</p>
</dd>
<dt class="hdlist1">bin/users.py</dt>
<dd>
<p>This is a command line script for managing JMdictDB users
(listing, adding, modifying, deleting, etc) that can be used as an
alternative to the "users.py" web page" or if the latter is inaccessible
for some reason.  It has been present for some time but was significantly
revised to eliminate some annoying mis-features.  Run 'bin/users.py --help'
for details.</p>
</dd>
<dt class="hdlist1">jmdictdb-pvt.ini</dt>
<dd>
<p>This is a user-created configuration file that
contains credentials that the JMdictDB system needs when connecting
to the Postgresql database.  Formerly, these credentials were
repeated in each database section.  It now allows them to be
specified only once in a DEFAULT section.  See section
<a href="install.html#cfg-pvtini">6.2.2 Create the jmdictdb-pvt.ini file</a>
in the Installation Guide and web/lib/jmdictdb-pvt.ini-sample file
for details.</p>
</dd>
<dt class="hdlist1">doc</dt>
<dd>
<p>A new <a href="dev.html">Development Guide</a>
was written containing information of interest to those who wish to
write or modify JMdictDB code or the database.  Some development related
information was moved into here from the other guides.  The other guides
have also undergone a series of continuing revisions.</p>
</dd>
<dt class="hdlist1">doc</dt>
<dd>
<p>The software upgrade procedure described in
<a href="oper.html">Operations Guide</a> has been revised.  Upgrading
from a preexisting development directory in no longer recommended.
Instead clone a fresh copy of the software when performing an upgrade.</p>
</dd>
<dt class="hdlist1">doc</dt>
<dd>
<p>In the <a href="install.html">Installation Guide</a>, the Python
package "bcrypt" has been added to the Requirements section.
This is used by the bin/users.py program.</p>
</dd>
<dt class="hdlist1">Makefile</dt>
<dd>
<p>The installation will now work with a default
install of Postgresql.  Formerly the Makefile and install scripts
assumed that Postgresql was configured for passworded access by
the "postgres" (database super-user) account and failed when access
was by the default "peer" method.  It now no longer requires
"postgres" user access at all and will work with any account
with CREATEDB and CREATEROLE privileges; and the latter is only
required briefly during the initial install.
This has only been lightly tested.</p>
</dd>
</dl>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="upd-apply">2. Applying the update</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="redirects">2.1. Redirect CGI URLs</h3>
<div class="paragraph">
<p>As mentioned at the top of this document, the CGI scripts from earlier
versions will no longer work reliably after the application of this
update.  Because the breakage is in some cases subtle and they may
appear to work, the Makefile used for installing/upgrading has been
adjusted to remove the installed CGI files.  (Only the files listed
in the Makefile&#8217;s CGI_FILES variable are removed; any other files
and the directory will not be.)</p>
</div>
<div class="paragraph">
<p>Users should be informed of and migrated to the new WSGI URLs or
redirects applied to the web server to redirect old CGI URLs to the
new WSGI ones before proceding with this upgrade.</p>
</div>
<div class="paragraph">
<p>The Apache configuration file (or section in a larger file) that defines
the CGI URLs should be removed.</p>
</div>
<div class="paragraph">
<p>Checking the Apache access log files over a period of time should show
no requests for the CGI URLs.</p>
</div>
</div>
<div class="sect2">
<h3 id="_code_and_database_update">2.2. Code and database update</h3>
<div class="paragraph">
<p>The code changes have merged into both the master and edrdg branches
so the usual upgrade procedure should be followed as described in
<a href="oper.html#upgrading">4. Upgrading JMdictDB</a>
section of the Operations Guide.</p>
</div>
<div class="admonitionblock note">
<table>
<tr>
<td class="icon">
<i class="fa icon-note" title="Note"></i>
</td>
<td class="content">
The upgrade procedure has been changed to recommend
upgrading from a freshly cloned copy of the JMdictDB software
instead of pulling the latest changes into an existing development
directory and running the upgrade from there.
</td>
</tr>
</table>
</div>
<div class="paragraph">
<p>This update requires applying the database updates:</p>
</div>
<div class="literalblock">
<div class="content">
<pre>044-7bb062  045-6f3d81</pre>
</div>
</div>
<div class="paragraph">
<p>(assuming the current database is at 043-2a6bd1, which it was at
the "edrdg"-tagged version.)  Updates need  to be applied to all
active JMdictDB databases (other than the jmtest01 database).</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_acknowledgments">3. Acknowledgments</h2>
<div class="sectionbody">
<div class="paragraph">
<p>I would like to thank Brian Krznarich for the concepts of applying
all edits to the head of the edit chain, marking overwritten changes
in the Edit Confirmation page entry history and Search page, and for
a very nice implementation in code.  I have rather mangled the code
since then and any errors rest with me, not Brian.</p>
</div>
</div>
</div>
</div>
<div id="footer">
<div id="footer-text">
Last updated 2023-09-06 20:19:10 -0600
</div>
</div>
</body>
</html>