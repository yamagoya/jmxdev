tests/web/

This directory contains test scripts that use the Selenium [*1]
web driver to automatate a web browser to access and test the
JBdictDB web pages by executing actual user interaction scenarios.

These tests can be run by runtests.py:

  $ cd {jmdictdb-dir}/tests/
  $ ./runtests.py --dir=web

Specific tests can be selecticed via arguments to the runtests.py command,
see the "Tests" section of the development Guide for details.

Most common reason for a test failure is receiving an Error page rather
than the expected page in a tests step.  For example, a test that expected
an the Edit Confirmation page but got the Error page instead will fail
with:

  Traceback (most recent call last):
    File "/home/stuart/devel/jdb/jb/tests/web/test_pages.py", line 186, in test_xxxxx
      chk_title (_, None, "Confirm Submission")
    File "/home/stuart/devel/jdb/jb/tests/web/test_pages.py", line 228, in chk_title
      _.assertEqual (prefix+title, got)
  AssertionError: 'JMdictDB - Confirm Submission' != 'JMdictDB - Error'
  - JMdictDB - Confirm Submission
  + JMdictDB - Error

To debug the problem run runtests.py with the -d (debug) option:

  $ ./runtests.py -d --dir=web

When the error occurs, runtests.py will enter the Python "pdb" debugger.
The web browser will show the error page and the "back" button can be used 
to look at previous pages.

The tests are run using the jmtest01 database and pages served from the
Flask debug web server (tools/run-flask.py).  It may be started with a
config file that enables "debug" level logging across the board or
restricted via filters to the components suspected of being the source
of faults.  The loging output (usually to the terminal window running
run-flask.py) will have logging output as well as any Python stack dumps
generated during the test.

Deeper debugging will generally require inserting pdb breakpoint calls 
(pdb.set_trace()) in the test code or in the relevant library code and
stepping through the problem code with the debugger.

----
[*1] https://selenium.dev
