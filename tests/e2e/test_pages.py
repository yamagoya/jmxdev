import sys, unittest, os.path, time, urllib.parse, warnings, pdb
import unittest_extensions, jmdb
from jmdb import  NoChange
from jmdictdb import jdb, db; from jmdictdb.objects import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

  # ['global' statements below have no effect; only here for documentation
  #  since conventionally readers are used to looking for such names near
  #  top of program as imports.]
global mkentr, addentr, edentr, addedit, dbread, dbwrite, dbentr, delentr
  # Retrieved from jmdb.EntryMaker.export() in setUpModule() below.
global elem, elems
  # Shorthand for the Selenium session functions find_element() and
  # find_elements() with the first parameter bound to By.CSS_SELECTOR.
  # They are made global in setUpModule().

  # Database name and load file for tests.
DBNAME, DBFILE = "jmtest01", "data/jmtest01.sql"
URLBASE = "http://localhost:5000"
URLSVC  = DBNAME
  # Following file should contain JMdictDB login credentials for a test user
  # with Editor privileges.  Specifically, three lines with: test userid, test
  # user's full name (with matching case), test user's password.
TEST_USER_FILE = './testuser.pw'    # Relative to the main tests/ directory.
CSS = By.CSS_SELECTOR   # For brevity.

#=============================================================================

class Pages (unittest.TestCase):        # Test accessibilty of basic pages.
    def setUp(_): Session.delete_all_cookies()  # Force log out.
    def test_root(_):
        chk_title (_, "", "Advanced Search")                 # Root url.
    def test_srchform(_):
        chk_title (_, "srchform.py", "Advanced Search", 'same')
    def test_srchformq(_):
        chk_title (_, "srchformq.py", "Basic Search", 'same')
    def test_edform(_):
        chk_title (_, "edform.py", "Edit Entry", 'same')     # New entry.
    def test_submissions(_):
        chk_title (_, "updates.py", "Recent Updates", 'same', params="i=1")
    def test_edhelp(_):
         chk_title (_, "edhelp.py", "Help", 'same')
    def test_entr(_):
         chk_title (_, "entr.py", "Entries", 'same', params="e=35")

class EditCycle (unittest.TestCase):   # Walk through steps of submitting
                                       #  new entry and approval by editor.
    def setUp(_): Session.delete_all_cookies()  # Force log out.
    def test_new_entry(_):
        testuserid, testusername, testuserpw = get_testuser (TEST_USER_FILE)
        s = Session   # For brevity.

          # Get new entry form and add an entry.
        Session.get (u('edform.py'))
          # Select "test" corpus.
        select (elem ('select[name=src]'), 'test')
          # Add reading text.
        rdng = elem ('textarea[name=rdng]')
        rdng.send_keys ("テストこうもく")
          # Add meaning text.  Since we don't clear the control, the
          # initial default text of "[1][n]" is still there, and this
          # appends to it.
        sens = elem ('textarea[name=sens]')
        sens.send_keys (" test entry")
          # Add a submitter name.  Use the test name and process id so
          # it can be easily found later for deletion.
        name = elem ('input[type=text][name=name]')
        submitter = sig(_)
        name.send_keys (submitter)
          # Click the Next button.
        button = elem ('div.jmd-content input[type=submit]')
        button.click()

          # edconf.py form...
        form = elem ('form[action="edsubmit.py"]')
          # Limit search for Submit button to the "edconf" form to prevent
          # finding the submit button for login/logout.
        button = form.find_element (CSS, 'input[type=submit]')
          # Click the Submit button.
        button.click()

          # submitted.jinja page...
          # Check that we got the Submission Successful page and extract
          # the corpus name, entry sequence number, id number and entry
          # link from it.
        corpus, seqnum, idnum, link = page_Submitted(_)
          # Click the id# link:
        link.click()

          # Entries form...
        chk_title (_, None, "Entries")
        itm = elem ('div.jmd-content > div.item > span.abbr')
        _.assertEqual (corpus, itm.text)
        itm = elem ('div.jmd-content > div.item > a')
        _.assertEqual (seqnum,itm.text)
        itm = elem ('div.jmd-content > div.item span.pend')
        _.assertEqual ("Active pending", itm.text)
        itm = elem ('div.jmd-content > div.item span.pkid a')
        _.assertEqual (idnum, itm.text)

          # Login as editor
        page_Login(_, testuserid, testuserpw, testusername)

          # edform.py...
        chk_title (_, None, "Edit Entry")
          # Add kanji text.
        kanj = elem ('textarea[name=kanj]')
        kanj.send_keys ("テスト項目")
          # Change submitter name.
        name = elem ('input[type=text][name=name]')
        submitter = sig(_)
        name.send_keys (submitter)
          # Click the Next button.
        button = elem ('div.jmd-content form[name=edconf] input[type=submit]')
        button.click()

          # edconf.py form...
        chk_title (_, None, "Confirm Submission")
          # Click the Submit button.  Since we are logged in as an editor
          # this will approve the entry by default.
        button = elem ('form[action="edsubmit.py"] input[type=submit]')
        button.click()

          # submitted.jinja page...
          # Check that we got the Submission Successful page and extract
          # the corpus name, entry sequence number and id number from it.
        corpus_a, seqnum_a, idnum_a, link = page_Submitted(_)

          # Final verification and clean up...
        _.assertEqual (corpus, corpus_a)
        _.assertEqual (seqnum, seqnum_a)
        _.assertNotEqual (idnum, idnum_a)
        _.assertFalse (dbread (e=idnum))  # Entry 'idnum' no longer exists,
        entrs = dbread (e=idnum_a)        #  it was replaced by 'idnum_a'.
        _.assertEqual (1, len(entrs))
          # Confirm entry is approved.  Because the entry row is read as a
          #  tuple, the .unap column is access by position [5].
        _.assertFalse (entrs[0][5])

        delentr (idnum)
        delentr (idnum_a)

class NoForkEdits (unittest.TestCase):
    def setUp(_): Session.delete_all_cookies()  # Force log out.
    def test_nf0010(_):
          # Create edits: e0 <-- e1*
          # Then edit e0 via web page to get:
          #               e0 <-- e1* <-- e2*
          # Should get warning on confirmation page that e2 will lose
          # changes made in e1.
        h0 = Hist(stat=2,unap=True,dt=db.DEFAULT,name=sig(_),notes='e0')
        e0 = addentr ('\fかえばえ\f[1][n]edit0', a=True, h=[h0])
        h1 = Hist(stat=2,unap=True,dt=db.DEFAULT,name=sig(_),notes='e1')
        e1 = edentr(e0,h=h1); e1._sens[0]._gloss[0].txt="edit1"; dbwrite (e1)

          # Get the Edit form for entry e0.
        Session.get (u('edform.py', params="e=%s" % e0.id))
          # Use different gloss text than first edit because we will want
          # to confirm that the history diff is against edit1 and not the
          # original entry.
        sens = elem ('textarea[name=sens]')
        sens.clear()
        sens.send_keys ("[1][n] edit2")
          # Add a submitter name.  Use the test name and process id so
          # it can be easily found later for deletion.
        name = elem ('input[type=text][name=name]')
        submitter = sig(_)
        name.send_keys (submitter)
          # Click the Next button.
        button = elem ('div.jmd-content input[type=submit]')
        button.click()

          # edconf.py form...
        chk_title (_, None, "Confirm Submission")
          #FIXME: there could (not in this specific test but in general) be
          # multiple warning messages and the edit fork warning might not be
          # the first as this check assumes.
        warnmsg = elem ('div.jmd-content div.warnings p.warnings').text
        _.assertIn ('Warning: Other edits have been made', warnmsg)
        rows = elems ('tbody.histtab tr')
        hhdrs, cautions = [], []
        for n,r in enumerate (rows):
            if "hhdr" in r.get_attribute ("class"): hhdrs.append (n)
            if "caution" in r.get_attribute ("class"): cautions.append (n)
          # There should 3 history header rows, one for each history record.
        _.assertEqual ([0,2,4], hhdrs)
          # Only history table row 2 (which is the header row for the "edit1"
          # entry) should have class "caution".
        _.assertEqual ([2], cautions)
          # Check the diffs and/or comments for each history record.
        sel = 'td:nth-child(3) pre'
        expect = "-<gloss>edit1</gloss>\n+<gloss>edit2</gloss>"
          # History record 1, diff.
        _.assertIn (expect,  elem(sel, rows[1]).text)
          # History record 2, comment.
        _.assertEqual ("e1", elem(sel, rows[3]).text)
          # History record 3, comment.
        _.assertEqual ("e0", elem(sel, rows[5]).text)
         # Click the Submit button.
        button = elem ('form[action="edsubmit.py"] input[type=submit]')
        button.click()

           # submitted.jinja page...
        corp, seq, id, link = page_Submitted (_)
          # IndexError on next statement indicates entry 'id' not found.
        e2 = dbentr (e=id)[0]
          # e2 (the entry edited from e0) should now be based on e1, not e0.
        _.assertEqual (e1.id, e2[4])  # e2[4]==e2.dfrm.
          # This is only one history record (produced by the submission of
          # e2) since e0 and e1 have none (the EntryMaker function don't
          # create them by default) but it should show gloss changing from
          # "edit1" to "edit2" since the edit is applied to head entry
          # (e1).  Note that the items in _hist are ordered from oldest
          # to newest, opposite the order displayed in the web pages so
          # most recent history item is _hist[2], not _hist[0].
        _.assertEqual (3, len (e2._hist))
        expect = "-<gloss>edit1</gloss>\n+<gloss>edit2</gloss>"
        _.assertIn (expect, e2._hist[2].diff)
          #FIXME: could use a little more extensive testing of the submitted
          # entry.

#=============================================================================

class XrefRealign (unittest.TestCase):
    def setUp(_): Session.delete_all_cookies()  # Force log out.
    def test_00010(_):
      # When entry is edited to swap order or two readings, preexistng xref
      # referencing one of those reading should still point to same (by text)
      # one in the new edit.

          # Create approved entry with two readings.
        e1 = addentr ("\fエックスあ;エックスい\f[1][n]x", a=True)
          # Create approved entry with xref to rdng 1 of above entry.
        e2 = mkentr ("\fワイ\f[1][n]y", a=True)
        e2._sens[0]._xref = [Xref(typ=3,xentr=e1.id,xsens=1,rdng=1)]
        dbwrite (e2)

          # Edit first entry (e1) and swap order of readings.
        Session.get (u('edform.py', params="e=%s"%e1.id))
        rdng = elem ('textarea[name=rdng]')
        rdng.clear();
        rdng.send_keys ("エックスい;エックスあ")
          # Provide submitter name.
        name = elem ('input[type=text][name=name]')
        submitter = sig(_)
        name.send_keys (submitter)
          # Click the Next button to go to Edit Confirmation page.
        button = elem ('div.jmd-content input[type=submit]')
        button.click()

          # edconf.py form...
        chk_title (_, None, "Confirm Submission")
          # Click the Submit button.
        button = elem ('form[action="edsubmit.py"] input[type=submit]')
        button.click()

          # submitted.jinja page...
        chk_title (_, None, "Submission successful")
          # We expect 2 rows (header and one data row for the submitted
          # entry) of 3 columns each but page could potentially report
          # more than one submitted entry so check that is not the case.
        itms = elems ('div.jmd-content table tr td')
        _.assertEqual (6, len(itms))
          # Get the new entry's corpus name, seq# and id#.  We need the
          # latter in order to delete the entry when done.
        itms_a = elems ('div.jmd-content table tr:nth-child(2) td')
        corpus, seqnum, idnum = [x.text for x in itms_a]

        e2 = dbentr (e=e2.id)[0]   # Get e2 from database.
          # We expect two xrefs on e2, 1) pointing to the original, unedited
          # e1, and 2) pointing to the new, edited e1.  The former should be
          # referencing rdng#1 as created, the second should have been
          # adjusted to rdng#2.  Former will also be reverse xref on original
          # e1, latter will also be reverse xref on new x1.
        e1new = dbentr (e=idnum)[0]  # Is the "readings swapped" edit of e1.
          # The xref on e2 that was originally pointing to reading #1 should
          # now, after readings were swapped above, be pointing to reading #2.
          # We check the reverse xref on the new e1.
        _.assertEqual (1, len (e1new._sens[0]._xrer))        # One xref only.
        _.assertEqual (e2.id, e1new._sens[0]._xrer[0].entr)  # From e2.
        _.assertEqual (2, e1new._sens[0]._xrer[0].rdng)      # To rdng#2.

##############################################################################
# Support functions

def page_Login(_, userid, pw, fullname):
          # Login to a JMdictDB page with 'userid' and password 'pw',
          # 'fullname' is used only to confirm successful login.
        ctl = elem ('div.jmd-header form input[name=username]')
        ctl.send_keys (userid)
        ctl = elem ('div.jmd-header form input[name=password]')
        ctl.send_keys (pw)
        elem ('div.jmd-header form input[type=submit]').click()
        got_name = elem ('div.jmd-header form a').text
        _.assertEqual (fullname, got_name)
          # Click Edit button
        elem ('div.jmd-content form input[type=submit]').click()

def page_Submitted(_):
        chk_title (_, None, "Submission successful")
          # We expect 2 rows (header and one data row for the submitted
          # entry) of 3 columns each but page could potentially report
          # more than one submitted entry so check that is not the case.
        itms = elems ('div.jmd-content table tr td')
        _.assertEqual (6, len(itms))
          # Get the new entry's corpus name, seq# and id#.  We need the
          # latter in order to delete the entry when done.
        itms = elems ('div.jmd-content table tr:nth-child(2) td')
          # Get the link to the new entry.
        link = itms[2].find_element (CSS, "a")
        corpus, seqnum, idnum = [x.text for x in itms]
        return corpus, seqnum, idnum, link

def select (element, item):
        selelem = Select (element)
        selelem.select_by_visible_text (item)

  # Create the URL for a page using URLBASE and URLSVC.
def u (page, params=[], urlbase=URLBASE, urlsvc=URLSVC):
          # 'params' may be a list of 2-tuples (eg [('e','2275041')]) or
          # a str (eg "e=2275051").
        scheme,netloc,path,*_ = urllib.parse.urlsplit (urlbase)
        path = '/'.join ([path,page])
        if isinstance (params, str):
            params = urllib.parse.parse_qsl (params)
        p = [('svc',urlsvc)] + params
        query = urllib.parse.urlencode (p)
        url = urllib.parse.urlunsplit ([scheme, netloc, path, query, ''])
        return url

  # Helper function to do simple verification of received page.
def chk_title (_, page, title, heading=None, params=[], prefix="JMdictDB - "):
          # Check that the page title, and optionally the <h2> heading text,
          # is as expected.
          #   page -- Page to check (eg, "srchform.py")
          #   title -- Expected title sans the value of 'prefix'.  Since
          #     'prefix' is the same for most pages, this reduces clutter
          #     in the calls.
          #   heading -- Expected value of the <h2> heading at the top of
          #     each JMdictDB page.
          #   params -- Any extra URL parameter values needed for the page.
          #     Can be given as either a list of name-value 2-tuples (eg
          #     ['e','35']) or a string (eg "e=35").
          #   prefix -- Default text appearing at the front of most pages
          #     titles (see 'titles' above).  May be overridden with this
          #     parameter.
        if page is not None: Session.get (u (page, params=params))
        got = Session.title
        _.assertEqual (prefix+title, got)
        if heading:
            if heading == "same": heading = title
            got = Session.find_element (By.CSS_SELECTOR, ".jmd-header h2").text
            _.assertEqual (heading, got)

def sig (_, funcname=''):
          # Provide an identifier that can be used when creating test
          # entries so they can be identified for deletion later.
        pid = " [" + str(os.getpid()) + "]"
        if funcname: funcname = "." + funcname
        return ("tests." + str(_.__class__)[8:-2] + funcname + pid)

def get_testuser (fname):
        try:
            with open (fname) as f: data = f.read()
        except OSError as e:
            print ("\n%s\n%s" % (str(e).strip(), ErrMsg % fname),
                   file=sys.stderr)
            raise
        testuserid, testusername, testuserpw = data.strip().split('\n')
        return testuserid, testusername, testuserpw

ErrMsg = """\
Unable to read test userid and password.
Expected a file, %s, containing two lines, the first with
the userid of the JMdictDB user (with editor privilege) the tests will
log into JMdictDB as, and the second with password for that user.  The
file must be readable by the operating system user running the tests."""

def setUpModule():
          # Open and save a database connection.
        global DBcursor
        DBcursor = jmdb.DBmanager.use (DBNAME, DBFILE)

          # Clean up any test corpus entries in the database.  This makes
          # running simultaneous copies if these tests in the same database
          # problematic.
        db.ex (DBcursor.connection,
               "DELETE FROM entr WHERE src=99; COMMIT")

          # EntryMaker is a bundle of helper functions for reading/writing
          # test entries to the test database.  We make them accessible
          # globally and with short names for brevity (and compatibilty
          # with earlier code when they were functions).
        em = jmdb.EntryMaker (DBcursor)
        global mkentr, addentr, edentr, addedit,\
          dbread, dbwrite, dbentr, delentr
        mkentr, addentr, edentr, addedit,\
          dbread, dbwrite, dbentr, delentr = em.export()

        # Selenium setup...
          # Ignore warnings from Selenium webdriver about unclosed log file
          # and tracemalloc.
        warnings.filterwarnings ("ignore", category=ResourceWarning)
          # Opening a web browser is very time consuming (several seconds) so
          # only do it once and reuse the browser instance for all tests.
        global Session, elem, elems
        Session = webdriver.Firefox()
          # The Selenium WebDriver .find_element() method with the
          # By.CSS_SELECTOR strategy is used frequently but is excesively
          # long to type, so we define a shorthand function, elem() to do
          # the same thing.  And the same for .find_elements().
        elem = lambda x,el=Session: el.find_element (CSS, x)
        elems = lambda x,el=Session: el.find_elements (CSS, x)

def tearDownModule():
        Session.quit()

if __name__ == "__main__": unittest.main()
