import sys, unittest, pdb
import unittest_extensions, jmdb
from jmdb import  NoChange
from jmdictdb import jdb, db

from jmdictdb import submit   # Function to test is submit.prepare().

  # Database name and load file for tests.
DBNAME, DBFILE = "jmtest01", "data/jmtest01.sql"

  # Global variables.
DBcursor = None	        # Set in setUpModule().
  # [Following 'global' statements have no effect; only for documentation.]
global mkentr, addentr, edentr, addedit, dbread, dbwrite, dbentr, delentr
  # Above functions are retrieved from jmdb.EntryMaker.export() and assigned
  # in setUpModule() below.

class Errors (unittest.TestCase):
    def test00010(_):  # No corpus (.src) value in entry.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=None)
        rv = submit.prepare (DBcursor, e, disp='', errs=errs)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('no corpus value selected', errs[0].lower())

    def test00020(_):  # Unselected corpus value in entry.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=0)
        rv = submit.prepare (DBcursor, e, disp='', errs=errs)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('no corpus value selected', errs[0].lower())

    def test00040(_):  # Non-existent parent.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=99, d=99999999)
        rv = submit.prepare (DBcursor, e, disp='', errs=errs)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('[noroot] ', errs[0].lower())

    def test00050(_):  # Reject entry by non-editor.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=99)
        rv = submit.prepare (DBcursor, e, disp='r', errs=errs, is_editor=False)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('only logged in editors can', errs[0].lower())

    def test00052(_):  # Approve entry by non-editor.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', d=19)
          # Entry #19 in jmtest01 is 1013970/アイス (active approved).
        rv = submit.prepare (DBcursor, e, disp='a', errs=errs, is_editor=False)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('only logged in editors can', errs[0].lower())

    def test00060(_):  # Reject approved entry (includes rejected and
                       #  deleted entries when they are approved though
                       #  those cases are not explicitly tested.)
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=99, a=True, d=116)
          # Entry #116 in jmtest01 is 1526080/万事/ほんじ (active/appr).
        rv = submit.prepare (DBcursor, e, disp='r', errs=errs, is_editor=True)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('can only reject unapproved entries', errs[0].lower())

    def test00060(_):  # Edit rejected entry.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=99, a=True, d=109)
          # Entry #109 in jmtest01 is 2833900/亜亜/ああ (rejected/appr).
        rv = submit.prepare (DBcursor, e, disp='', errs=errs, is_editor=True)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('rejected entries can not be edited', errs[0].lower())

    def test00070(_):  # New entry with delete requested.
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', c=99, s=4)  # 4 is delete.
        rv = submit.prepare (DBcursor, e, disp='', errs=errs)
        entr, pentr, edchain, opid, root_unap = rv
        _.assertIsNone (entr);  _.assertEqual (1, len(errs))
        _.assertIn ('delete requested but this is a new', errs[0].lower())

    def test00080(_):  # Approve non-head entry.  (Also DB contraint vio.)
        errs, e = [], mkentr ('犬\fいぬ\f[1][n]dog', d=104)
          # Entry #104 in jmtest01 is 2833900/亜亜/ああ (has other pending edit)
        rv = submit.prepare (DBcursor, e, disp='a', errs=errs, is_editor=True)

#=============================================================================
# Support functions.

def setUpModule():
        global DBcursor
        DBcursor = jmdb.DBmanager.use (DBNAME, DBFILE)
        em = jmdb.EntryMaker (DBcursor)
        em = jmdb.EntryMaker (DBcursor)
          # Allow access to EntryMake methods as functions for brevity and
          # compatibilty with earlier revs when they were functions.
        global mkentr, addentr, edentr, addedit,\
          dbread, dbwrite, dbentr, delentr
        mkentr, addentr, edentr, addedit,\
          dbread, dbwrite, dbentr, delentr = em.export()
        db.ex (DBcursor.connection,
               "DELETE FROM entr WHERE src=99; COMMIT")

def tearDownModule():
        #print ("Deleting entries from 'test' corpus", file=sys.stderr)
        DBcursor.connection.rollback()   # In case a test fail left an
                                         #  open aborted transaction.
          # Remove any test entries that we couldn't avoid committing.
        #db.ex (DBcursor.connection,
        #       "DELETE FROM entr WHERE src=99; COMMIT")

def main(): unittest.main()

if __name__ == '__main__': unittest.main()
