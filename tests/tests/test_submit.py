import sys, unittest, os.path, copy, pdb
import unittest_extensions, jmdb
from jmdb import  NoChange
from jmdictdb import jdb, db; from jmdictdb.objects import *

from jmdictdb import submit   # Module to test

  # Database name and load file for tests.
DBNAME, DBFILE = "jmtest01", "data/jmtest01.sql"

  # Global variables.
DBcursor = None	        # Set in setUpModule().
  # [Following 'global' statements have no effect; only for documentation.]
global mkentr, addentr, edentr, addedit, dbread, dbwrite, dbentr, delentr
  # Above functions are retrieved from jmdb.EntryMaker.export() and assigned
  # in setUpModule() below.
global elem, elems
  # These are shorthand for the Selenium session functions find_element()
  # and find_elements() with the first parameter bound to By.CSS_SELECTOR.
  # They are made global in setUpModule().

  # These tests are not intended to be comprehensive but test a few
  # common cases in the context of a complete submission.
  #
  # These tests create entries in the "test" corpus (src=99) in
  # order not to change any of the entries in other corpora which
  # could invalidate the database for further tests.
  #
  # The tests all share the same database connection (via global
  # 'DBcursor') so sequential operations (eg submit an entry then
  # read it) are in the same transaction and the first operation
  # need not do a commit for the second to see its changes.
  #
  # Conversly a failure of any test will leave the database connection
  # in a failed transaction state which will break any following tests.
  # This is avoided by having a setUp() method in each class, which
  # is run before each test, do a rollback.
  #
  # Both at start of this module's tests and again after they've all
  # run, module setup and teardown functions delete all the entries in
  # the test corpus so preexisting detritus won't affect these tests
  # and to leave things clean for tests that use the database afterwards.
  #
  # A number of tests create the entries in the test database that are
  # then edited and submitted.  The entries are (in some cases) created
  # directly in the database by local helper function addentr() (a thin
  # wrapper around jdb.addentr()) without going through submit.submission().
  # It is then convenient to "edit" the returned entr object (since it
  # already has kanj, rdng, sens, etc, values set) my modifying it and
  # passing it to submission() to check for the expected response.  This
  # editing is conveniently done with helper function edentr() which will
  # automatically create the necessary .dfrm link to the entry it is
  # editing.

class General (unittest.TestCase):
    def setUp(_):  # In case a test fail left an open aborted transaction.
        DBcursor.connection.rollback()

    def test0000100(_):  # Check for presence of db update 6f3d81 (enforces
          # non-forking edit at database level.)  This is unlikely to occur
          # since the jmtest01 database was committed with the update but
          # it may have been temporarily disabled for some reason.
        e0 = addentr ('\fパン\f[1]bread', c=99, s=2)
        e1 =  addedit(e0)       # Add edit of e0.
          # Atempting to add another edit of e0 will trigger the database
          # constraint that prohibits two entries with the same .dfrm value.
        p = 'duplicate key .* violates unique constraint "entr_dfrm_idx"'
        m = "Database update 6f3d81 appears to be missing."
        with _.assertRaisesRegex (db.IntegrityError,p,msg=m): addedit(e0);

    def test1000010(_):  # Sub-minimal new entry: no sense, reading or kanji.
        errs = []
        e = Entr (src=99, stat=2)
        submit.submission (DBcursor, e, '', errs)
        _.assertEqual (3, len(errs))
        errtxts = set ([t[:24].lower() for t in errs])
        _.assertIn ('no readings were entered', errtxts)
        _.assertIn ('both the kanji and readi', errtxts)
        _.assertIn ('no senses given.  you mu', errtxts)

    def test1000020(_):  # Minimal submission requires (for JMdict) at least
                         # a reading, gloss and PoS.  Constructed from raw
                         # objects as demo.  Rest will use adderr, mkentr,...
        inp = Entr (stat=2, src=99,
                    _rdng=[Rdng(txt='ゴミ')], _kanj=[],
                    _sens=[Sens(_gloss=[Gloss(txt="trash",lang=1,ginf=1)],
                                _pos=[Pos(kw=jdb.KW.POS['n'].id)],)],)
        es = submit_ (inp, disp='')
        eid, seq, src = es.id, es.seq, es.src
        for t in eid,seq,src: _.assertTrue(t)
          # An index exception on next line indicates the new entry
          #  was not found.  Note that we do not have to commit it;
          # because we read with the same connection it was written
          # with the read is in the same transaction.
        out = jdb.entrList (DBcursor, None, [eid])[0]
          # Note that submit.submission() modified 'inp' to set all the
          #  object primary key fields to the values used in the database
          #  and thus it should exactly match 'out'.
        _.assertEqual (inp, out)

    def test1000040(_):  # Parent entry disappears before commit of child
                         #  as can happen when someone else approves a
                         #  different edit of the same entry.
        e1 = addentr ('\fパン\f[1][n]bread', c=99, s=2)  # Add an entry
        e2 = edentr (e1)                                 # and edit it.
          # But before it's submitted, remove the parent entry as would
          # happen if someone else had approved a different edit of it.
        delentr (e1.id)
          # Now submit our first edit which should raise an exception
          # since the parent entry is gone.
        errs = submitE (_, e2, disp='')
        _.assertIn ('[noroot] The entry you are editing no longer exists',
                    errs[0])

    def test1000110(_):  # Non-editor can't edit rejected entry.
        e1 = addentr ('\fパン\f[1][n]bread', c=99, s=6)  # Add rejected entry
        e2 = edentr (e1, s=2)                            # and edit it.
        errs = submitE (_, e2, disp='')
        _.assertIn ("Rejected entries can not be edited", errs[0])

    def test1000120(_):  # Editor can't edit rejected entry.
        e1 = addentr ('\fパン\f[1][n]bread', c=99, s=6)  # Add rejected entry
        e2 = edentr (e1, s=2)                            # and edit it.
        errs = submitE (_, e2, disp='', is_editor=True)
        _.assertIn ("Rejected entries can not be edited", errs[0])

    def test1000130(_):  # Edit of a non-existant entry (same as test1000040?)
        e0 = mkentr ('\fパン\f[1][n]bread', c=99, s=2, d=9999999999)
        errs = submitE (_, e0, disp='')
        _.assertIn ('[noroot] The entry you are editing no longer exists',
                    errs[0])


class Approval (unittest.TestCase):
    def setUp(_):  # In case a test fail left an open aborted transaction.
        DBcursor.connection.rollback()

    def test1000010(_):  # Create an new unapproved entry and approve.
        inp = addentr ('\fパン\f[1][n]bread', c=99)
        es = submit_ (inp, disp='')
        eid, seq, src = es.id, es.seq, es.src
        DBcursor.connection.commit()
          # An index exception on next line indicates the new entry
          #  was not found.
        out = jdb.entrList (DBcursor, None, [eid])[0]
          # Make a change to the entry.
        inp._sens[0]._gloss[0].txt = "bread"
        inp.dfrm = eid
          # And submit with editor approval.
        es2 = submit_ (inp, disp='a',is_editor=True,userid='smg')
        eid2, seq2, src2 = es2.id, es2.seq, es2.src
        out2 = jdb.entrList (DBcursor, None, [eid2])[0]
        _.assertEqual (inp, out2)
          # The original entry should have been disappeared.
        out3 = jdb.entrList (DBcursor, None, [eid])
        _.assertEqual ([], out3)
        #FIXME? should also check history records?

    def test1500020(_):  # Edit an approved entry and approve.
        inp = Entr (stat=2, src=99,
                    _rdng=[Rdng(txt='パン')], _kanj=[],
                    _sens=[Sens(_gloss=[Gloss(txt="breab",lang=1,ginf=1)],
                                _pos=[Pos(kw=jdb.KW.POS['n'].id)],)],)
          # Create a new, approved entry.
        es = submit_ (inp, disp='a',is_editor=True,userid='smg')
        eid, seq, src = es.id, es.seq, es.src
          # An index exception on next line indicates the new entry
          #  was not found.
        out = jdb.entrList (DBcursor, None, [eid])[0]
          # Make a change to the entry.
        inp._sens[0]._gloss[0].txt = "bread"
        inp.dfrm = eid
          # And submit with editor approval.
        es2 = submit_ (inp, disp='a',is_editor=True,userid='smg')
        eid2, seq2, src2 = es2.id, es2.seq, es2.src
        out2 = jdb.entrList (DBcursor, None, [eid2])[0]
        _.assertEqual (inp, out2)
          # The original entry should have been disappeared.
        out3 = jdb.entrList (DBcursor, None, [eid])
        _.assertEqual ([], out3)

    def test1501030(_):  # Fail when submit entries with same corpus/seq#.
                         # IS-213.
        e1 = addentr ('\fパン\f[1][n]bread', q=1501030, a=True)
          # Create a second, independent (because its .dfrm does not point
          # to e1), entry with same seq number...
        e2 = mkentr ('\fパンぼうし\f[1][n]bread hat', q=1501030)
          # ...then try to submit it.
        errs = submitE (_, e2, disp='a', is_editor=True, userid='smg')
        _.assertIn ('[seq_vio] The entry you are editing no longer exists',
                    errs[0])
        _.assertEqual (1, len(errs))   # Expect only 1 error message.

    def test1501040(_):  # Fail approve of a non-head edit.
          # Create edits: e1(A)-->e2(A*)-->e3(A*)
          # then try to approve e2.  It should fail because there is
          # a later edit on the edit chain.
        e1 = addentr ('\fねこいし\f[1][n]cat stone', a=True)
        e2 = addedit (e1)
        e3 = addedit (e2)
          # Edit e2 and and try to approve with submit.submission().
        e = edentr (e2)
        errs = submitE (_, e, disp='a', is_editor=True, userid='smg')
          # Verify error message and conflicting edit ids.
        _.assertIn ("You can only approve the latest edit", str(errs[0]))
        _.assertEqual (1, len(errs))   # Expect only 1 error message.

      #FIXME: currently an approved entry can be approved again.
    @unittest.expectedFailure
    def test1501050(_):  # Fail approve of an already approved entry.
          # Create edits: e1(A)
          # then try to approve e1.  It should fail because it's
          # already approved.
        e1 = addentr ('\fねこいし\f[1][n]cat stone', a=True)
          # Edit e1 and and try to approve with submit.submission().
        e = edentr (e1)
        errs = submitE (_, e, disp='a', is_editor=True, userid='smg')
          # Verify error message and conflicting edit ids.
        _.assertIn ('Other new edits were submitted before', errs[0])
        _.assertEqual (1, len(errs))   # Expect only 1 error message.


class Reject (unittest.TestCase):
    def setUp(_):  # In case a test fail left an open aborted transaction.
        DBcursor.connection.rollback()

    def test2000010(_):       # Reject new unapproved edit, c.f. test2001020
        e1 = addentr ("\fかえばえ\f[1][n]nonsense")
        e = edentr (e1)
        submit_ (e, disp='r',is_editor=True,userid='smg')
        f = dbread (q=e1.seq)
        _.assertEqual (1, len(f), "Expected 1 entry, got: %s" % f)
        _.assertEqual ((e1.src, 6, e1.seq, None, False), f[0][1:6])

    def test2000020(_):       # Reject a single edit to an approved entry.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e2 = addedit (e1)
        e = edentr (e2)
        submit_ (e, disp='r',is_editor=True,userid='smg')
        f = dbread (q=e1.seq)
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
        _.assertEqual ((e1.src, e1.stat, e1.seq, None, e1.unap), f[0][1:6])
        _.assertEqual ((e2.src, 6,       e2.seq, None, False),   f[1][1:6])

#   def test2000030(_):       # Reject a chain of edits to an unapproved entry.
#                             # Covered by tests test2002010 to test2002030.

#   def test2000040(_):       # Reject a chain of edits to an approved entry.
#                             # Covered by tests test2002130 to test2002140.

    def test2001020(_):       # Fail: reject approved entry, c.f. test2000010
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e = edentr (e1)   # Edit root entry for rejection.
        regex = "You can only reject unapproved entries"
        with _.assertRaisesRegex (RuntimeError, regex):
            submit_ (e, disp='r',is_editor=True,userid='smg')

    def test_2001050(_):      # Rejected entry should not include user edits.
          # Create e1 <-- e2*;  edit e2 and change gloss, then reject.
          # The rejected entry should have original gloss of e1 and e2,
          # not the changed gloss of the edited version of e2 (e).
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e2 = addedit (e1);
        e = edentr (e2); e._sens[0]._gloss[0].txt = "changed"
        es = submit_ (e, disp='r',is_editor=True,userid='smg')
        er = dbentr (e=es.id)
        _.assertEqual ("nonsense", er[0]._sens[0]._gloss[0].txt)
        _.assertEqual ("nonsense", es._sens[0]._gloss[0].txt)

    # Rejection tests under new "nofork" behavior...

    def test2002010(_):
          # A* <-- B* <-- C*    reject C*;  expect: A*<--B*, Cr
        A = addentr ("\fかえばえ\f[1][n]root")
        B = addedit (A);  C = addedit (B);  Cid = C.id;
        e = edentr (C)
        submit_ (e, disp='r',is_editor=True,userid='smg')
        f = dbread (q=A.seq)
        _.assertEqual (3, len(f), "Expected 3 entries, got: %s" % f)
        _.assertEqual ((99, 2, A.seq, None,  True),  f[0][1:6])
        _.assertEqual ((99, 2, A.seq, A.id,  True),  f[1][1:6])
        _.assertEqual ((99, 6, A.seq, None,  False), f[2][1:6])
        _.assertTrue (ishead (B.id))    # B still exists, is now head edit.
        _.assertTrue (isgone (Cid))     # C is now replaced by...
        _.assertTrue (ishead (f[2][0])) # ...the rejected version, Cr.
          #FIXME: how do we know new Cr (f[2]) is edit of old C and not
          # A or B?  Need to check hist records.

    def test2002020(_):
          # A* <-- B* <-- C*    reject B*;  expect: A*, Cr
        A = addentr ("\fかえばえ\f[1][n]root")
        B = addedit (A);  C = addedit (B);  Bid,Cid = B.id,C.id
        e = edentr (B)
        submit_ (e, disp='r',is_editor=True,userid='smg')
        f = dbread (q=A.seq)
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
        _.assertEqual ((99, 2, A.seq, None,  True),  f[0][1:6])
        _.assertEqual ((99, 6, A.seq, None,  False), f[1][1:6])
        _.assertTrue (ishead (A.id))    # A still exists, is now head edit.
        _.assertTrue (isgone (Bid))     # B and C are both gone.
        _.assertTrue (isgone (Cid))     # C is now replaced by...
        _.assertTrue (ishead (f[1][0])) # ...the rejected version.

    def test2002030(_):
          # A* <-- B* <-- C*    reject A*;  expect: Cr
        A = addentr ("\fかえばえ\f[1][n]root")
        B = addedit (A);  C = addedit (B);  Aid,Bid,Cid = A.id,B.id,C.id
        e = edentr (A)
        submit_ (e, disp='r',is_editor=True,userid='smg')
        f = dbread (q=A.seq)
        _.assertEqual (1, len(f), "Expected 1 entry, got: %s" % f)
        _.assertEqual ((99, 6, A.seq, None,  False), f[0][1:6])
        _.assertTrue (isgone (Aid))
        _.assertTrue (isgone (Bid))
        _.assertTrue (isgone (Cid))
        _.assertTrue (ishead (f[0][0]))

    def test2002120(_):
          # A <-- B* <-- C*    reject B*;  expect: A, Cr
        A = addentr ("\fかえばえ\f[1][n]root", a=True)
        B = addedit (A);  C = addedit (B);  Bid,Cid = B.id,C.id
        e = edentr (B)
        submit_ (e, disp='r',is_editor=True,userid='smg')
        f = dbread (q=A.seq)
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
        _.assertEqual ((99, 2, A.seq, None,  False), f[0][1:6])
        _.assertEqual ((99, 6, A.seq, None,  False), f[1][1:6])
        _.assertTrue (ishead (A.id))    # A still exists, is now head edit.
        _.assertTrue (isgone (Bid))     # B and C are both gone.
        _.assertTrue (isgone (Cid))     # C is now replaced by...
        _.assertTrue (ishead (f[1][0])) # ...the rejected version.

    def test2002130(_):
          # A <-- B* <-- C*    reject A;  expect: error
        A = addentr ("\fかえばえ\f[1][n]root", a=True)
        B = addedit (A);  C = addedit (B);  Aid,Bid,Cid = A.id,B.id,C.id
        e = edentr (A)
        errs = submitE (_, e, disp='r',is_editor=True,userid='smg')
        _.assertEqual (1, len(errs))   # Expect only 1 error message.
        _.assertIn ('can only reject unapproved entries', errs[0])

class Delete (unittest.TestCase):
    # Test submission of "delete" entries.  We  only test the basic edits
    # since the processing of branch edits and such is done be the same
    # code as for other submissions and is tested by other test classes.

    def setUp(_):  # In case a test fail left an open aborted transaction.
        DBcursor.connection.rollback()

    def test3000010(_):        # Unapproved delete of unapproved entry.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense")
        e = edentr (e1, s=4)   # Edit root entry for deletion.
        submit_ (e, disp='',is_editor=True,userid='smg')
        f = dbread (q=e1.seq)
          # We expect both the original approved entry 'e1' and the
          # new entry to exist.
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
                     # src stat  seq   dfrm     unap
        _.assertEqual ((99, 2, e1.seq, None,    True), f[0][1:6])
        _.assertEqual ((99, 4, e1.seq, e1.id,   True), f[1][1:6])

    def test3000020(_):        # Unapproved delete of approved entry.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e = edentr (e1, s=4)   # Edit root entry for deletion.
        submit_ (e, disp='',is_editor=True,userid='smg')
        f = dbread (q=e1.seq)
          # We expect both the original approved entry 'e1' and the
          # new entry to exist.
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
                     # src stat  seq   dfrm     unap
        _.assertEqual ((99, 2, e1.seq, None,   False), f[0][1:6])
        _.assertEqual ((99, 4, e1.seq, e1.id,   True), f[1][1:6])

    def test3000030(_):        # Approved delete of unapproved entry.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense")
        e = edentr (e1, s=4)   # Edit root entry for deletion.
        submit_ (e, disp='a',is_editor=True,userid='smg')
        f = dbread (q=e1.seq)
          # We expect the original unapproved entry 'e1' to be gone, and
          # the lone new approved (arg 5 below == False) entry to have
          # status 'D' (arg 2 below == 4).
        _.assertEqual (1, len(f), "Expected 2 entries, got: %s" % f)
                     # src stat  seq   dfrm     unap
        _.assertEqual ((99, 4, e1.seq, None,   False), f[0][1:6])

    def test3000040(_):        # Approved delete of approved entry.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e = edentr (e1, s=4)   # Edit root entry for deletion.
        submit_ (e, disp='a',is_editor=True,userid='smg')
        f = dbread (q=e1.seq)
          # We expect the original unapproved entry 'e1' to be gone, and
          # the lone new approved (arg 5 below == False) entry to have
          # status 'D' (arg 2 below == 4).
        _.assertEqual (1, len(f), "Expected 2 entries, got: %s" % f)
                     # src stat  seq   dfrm     unap
        _.assertEqual ((99, 4, e1.seq, None,   False), f[0][1:6])

    def test_3002010(_):      # Deleted entry should not include user edits.
          # Create e1 <-- e2*;  edit e2 and change gloss, then submit for
          # delete.  The delete entry (unapproved) should have original gloss
          # of e1 and e2, not the changed gloss of the edited e2 (e).
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e2 = addedit (e1);
        e = edentr (e2, s=4); e._sens[0]._gloss[0].txt = "changed"
        es = submit_ (e, disp='')
        er = dbentr (e=es.id)
        _.assertEqual ("nonsense", er[0]._sens[0]._gloss[0].txt)
        _.assertEqual ("nonsense", es._sens[0]._gloss[0].txt)


class Nofork (unittest.TestCase):  # Tests for "no fork" submit changes
                                   #  in "bk" branch (230323-6c57ca7).
    def test_3500010(_):
          # Create edits: e1(A)--.--->e2(A*)
          #                       `-->e3(A*)
          # Submit of e3 should fail because edit forks are prohibited.
        e1 = addentr ('\fかえばえ\f[1][n]nonsense', a=True)
        e2 = addedit(e1)
        e3 = edentr (e1)
        errs = submitE (_, e3, disp='')
        _.assertIn ("[dfrmuniq_vio] Other edits have been made", errs[0])


class History (unittest.TestCase):
    def setUp(_):  # In case a test fail left an open aborted transaction.
        DBcursor.connection.rollback()

    def test4000010(_):
          # Create a new unapproved entry from an approved entry with
          # no history.  This represents probably the most common edit
          # operation that occurs since the vast majority of JMdictDB
          # entries are approved with no history.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True)
        e = edentr (e1, h=Hist(name="hist-test"))
        submit_ (e, disp='')
        sql = "SELECT id FROM entr WHERE src=99 AND seq=%s"
        f = jdb.entrList (DBcursor, sql, (e1.seq,))
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
          # Original entry's history list should remain empty.
        _.assertEqual (0, len(f[0]._hist))
          # We expect a single item in the new entry hist list.
        _.assertEqual (1, len(f[1]._hist))
          # It should have the name we provided above.
        _.assertEqual ("hist-test", f[1]._hist[0].name)
          # The "eid" field should be same as the "entr" field.
        _.assertEqual (f[1]._hist[0].entr, f[1]._hist[0].eid)

    def test4000020(_):  # Appr entry w/hists -> unappr entry
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True,
                      h=[Hist(name="hist-1",stat=2,unap=True, dt=db.DEFAULT),
                         Hist(name="hist-2",stat=2,unap=False,dt=db.DEFAULT)])
        e = edentr (e1, h=Hist(name="hist-test"))
        submit_ (e, disp='')
        sql = "SELECT id FROM entr WHERE src=99 AND seq=%s"
          # Retrieve 'e1' and 'e' from the database as 'f[0]' and 'f[1]'.
        f = jdb.entrList (DBcursor, sql, (e1.seq,))
        _.assertEqual (2, len(f), "Expected 2 entries, got: %s" % f)
          # Original entry's history list should remain at two items.
        _.assertEqual (2, len(f[0]._hist))
          # We expect three items in the new entry hist list.
        _.assertEqual (3, len(f[1]._hist))
          # It should have the names we provided above.
        _.assertEqual ("hist-1",    f[1]._hist[0].name)
        _.assertEqual ("hist-2",    f[1]._hist[1].name)
        _.assertEqual ("hist-test", f[1]._hist[2].name)
          # When 'e1' was added to the database, jdb.addentr() set the last
          # hist item's .eid value but didn't touch its first.  When 'e' was
          # submmitted, it received e1 hists and added a new one; the new
          # one's .eid value was set by jdb.addentr().
        _.assertEqual (None, f[1]._hist[0].eid)
        _.assertEqual (f[0].id, f[1]._hist[1].eid)
        _.assertEqual (f[1].id, f[1]._hist[2].eid)

    def test4000030(_):  # Appr entry w/hists -> appr entry
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=True,
                      h=[Hist(name="hist-1",stat=2,unap=True, dt=db.DEFAULT),
                         Hist(name="hist-2",stat=2,unap=False,dt=db.DEFAULT)])
        e = edentr (e1, h=Hist(name="hist-test"))
        submit_ (e, disp='a', is_editor=True)
        sql = "SELECT id FROM entr WHERE src=99 AND seq=%s"
        f = jdb.entrList (DBcursor, sql, (e1.seq,))
          # We should get only one entry, the newly approved one.
        _.assertEqual (1, len(f), "Expected 1 entry, got: %s" % f)
        f = f[0]
          # We expect three items in the new entry hist list.
        _.assertEqual (3, len(f._hist))
          # It should have the names we provided above.
        _.assertEqual ("hist-1",    f._hist[0].name)
        _.assertEqual ("hist-2",    f._hist[1].name)
        _.assertEqual ("hist-test", f._hist[2].name)
          # When 'e1' was added to the database, jdb.addentr() set the last
          # hist item's .eid value but didn't touch its first.  When 'e' was
          # submmitted, it received 'e1' hists and added a new one; the new
          # one's .eid value was set by jdb.addentr().  When checking hist[1]'s
          # eid value, compare to our local 'e1' since 'e1' was deleted from
          # the database when 'e' was approved.
        _.assertEqual (None,    f._hist[0].eid)
        _.assertEqual (e1.id,   f._hist[1].eid)
        _.assertEqual (e.id,    f._hist[2].eid)
        _.assertEqual (f.id,    e.id)

    def test4000040(_):  # Unappr entry w/hists, multiple edits -> rej entry
          # Create a new entry followed by two edits.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=False,
                      h=[Hist(name="hist-1",stat=2,unap=True,dt=db.DEFAULT)])
        e2 = addedit (e1, h=Hist(name="hist-2",stat=2,unap=True,dt=db.DEFAULT))
        e3 = addedit (e2, h=Hist(name="hist-3",stat=2,unap=True,dt=db.DEFAULT))
          # Reject the most recent edit.  The results should be a rejected
          # entry of that edit, and e1, e2 left unchanged.
        e = edentr (e3, h=Hist(name="hist-test"))
        submit_ (e, disp='r', is_editor=True)
          # Read all the entries from the database.
        entrs = dbentr (q=e1.seq)
        _.assertEqual (3, len(entrs), "Expected 3 entries, got: %s" % entrs)
          # Get the rejected entry for examination.
        f = entrs[2]
          # We expect four items in the rejected entry's hist list, the
          # records for the thress edits
        _.assertEqual (4, len(f._hist))
          # It should have the names we provided above.
        _.assertEqual ("hist-1",    f._hist[0].name)
        _.assertEqual ("hist-2",    f._hist[1].name)
        _.assertEqual ("hist-3",    f._hist[2].name)
        _.assertEqual ("hist-test", f._hist[3].name)
        _.assertEqual (e1.id,       f._hist[0].eid)
        _.assertEqual (e2.id,       f._hist[1].eid)
        _.assertEqual (e3.id,       f._hist[2].eid)
        _.assertEqual (e.id,        f._hist[3].eid)
        _.assertEqual (f.id,        e.id)

    def test4000050(_):  # Unappr entry w/hists, multiple edits -> rej entry
          # Create a new entry followed by two edits.
        e1 = addentr ("\fかえばえ\f[1][n]nonsense", a=False,
                      h=[Hist(name="hist-1",stat=2,unap=True,dt=db.DEFAULT)])
        e2 = addedit (e1, h=Hist(name="hist-2",stat=2,unap=True,dt=db.DEFAULT))
        e3 = addedit (e2, h=Hist(name="hist-3",stat=2,unap=True,dt=db.DEFAULT))
          # Reject the original entry.  The results should be a single
          # rejected entry.
        e = edentr (e1, h=Hist(name="hist-test"))
        submit_ (e, disp='r',is_editor=True)
          # Read all the entries from the database.
        entrs = dbentr (q=e1.seq)
        _.assertEqual (1, len(entrs), "Expected 1 entry, got: %s" % entrs)
          # Get the rejected entry for examination.
        f = entrs[0]
          # We expect three items in the new entry hist list.
        _.assertEqual (4, len(f._hist))
          # It should have the names we provided above.
        _.assertEqual ("hist-1",    f._hist[0].name)
        _.assertEqual ("hist-2",    f._hist[1].name)
        _.assertEqual ("hist-3",    f._hist[2].name)
        _.assertEqual ("hist-test", f._hist[3].name)
        _.assertEqual (e1.id,       f._hist[0].eid)
        _.assertEqual (e2.id,       f._hist[1].eid)
        _.assertEqual (e3.id,       f._hist[2].eid)
        _.assertEqual (e.id,        f._hist[3].eid)
        _.assertEqual (f.id,        e.id)


class Clean (unittest.TestCase):
      # Tests for submit.clean() which strips ascii control characters
      # from a string and expands tabs.
    def test_0010(_): _.assertEqual (None, submit.clean (None))
    def test_0020(_): _.assertEqual ('', submit.clean (''))
    def test_0030(_): _.assertEqual ('a b', submit.clean ('a b'))
    def test_0040(_): _.assertEqual ('a\nb', submit.clean ('a\nb'))
    def test_0050(_): _.assertEqual ('a\nb', submit.clean ('a\r\nb'))
      # Tests 0060: currently tab expansion was reverted hence the change here.
    #def test_0060(_): _.assertEqual ('aa      b', submit.clean ('aa\tb'))
    def test_0060(_): _.assertEqual ('aa\tb', submit.clean ('aa\tb'))
    def test_0070(_): _.assertEqual ('ab', submit.clean ('a\bb\x1f'))

       # When given 'source' and 'errs' arguments it will record the
       # cleanup by adding a message to 'errs'.

    def test_0310(_):   # Nothing removed, no message.
         errs = []
         _.assertEqual ('ab', submit.clean ('ab', 'test', errs))
         _.assertEqual ([], errs);

    def test_0320(_):  # Make sure we get expected message and existing
         errs = ['x']  #  messages are not overwritten.
         _.assertEqual ('ab', submit.clean ('a\x0cb', 'test', errs))
         _.assertEqual ('x', errs[0]);
         _.assertEqual ("Illegal characters in 'test'", errs[1])

    def test_0330(_):  # In context of a submission, bad characters
        errs = []      #  abort it.
        e = Entr (src=99, stat=2,
                  _rdng=[Rdng(txt='ゴミ')], _kanj=[],
                  _sens=[Sens(_gloss=[Gloss(txt="trash",lang=1,ginf=1)],
                              _pos=[Pos(kw=jdb.KW.POS['n'].id)],)],
                  _hist=[Hist(notes='a\r\nb')])
        es = submit.submission (DBcursor, e, '', errs)
        _.assertEqual (None, es)
        _.assertEqual (1, len(errs))
        _.assertEqual ("Illegal characters in 'comments'", errs[0])


class Xrefs (unittest.TestCase):   # Named with plural form because "Xref"
    @classmethod                   #  conflicts with objects.Xref.
    def setUpClass(cls):
          # Create some entries to use as xref targets.  These are created
          # in the test corpus (c=99) since all entries in that corpus are
          # removed by the tearDownModule() function when all tests are
          # finished.
        cls.t1 = addentr ('猫\fねこ\f[1]cat', q=20100, c=99)
        cls.t2 = addentr ('馬\fうま\f[1]horse[2]mule', q=20110, c=99)
    def setUp(_):  # In case a test fail left an open aborted transaction.
        DBcursor.connection.rollback()

    def test0001(_):
          # Create a single, simple xref.
        e0 = mkentr ('犬\fいぬ\f[1][n]dog [see=猫]', c=99)
        es = submit_ (e0, disp='')
        id, seq, src = es.id, es.seq, es.src
        e1 = jdb.entrList (DBcursor, None, [id])[0]
        expect = [Xref (id,1,1,3,_.t1.id,1,None,1,None,False,False)]
        _.assertEqual (expect, e1._sens[0]._xref)
        _.assertEqual ([],     e1._sens[0]._xrslv)

    def test0002(_):
          # Xref to kanji and sense #1.
        e0 = mkentr ('犬\fいぬ\f[1][n]dog [see=馬[2]]', c=99)
        es = submit_ (e0, disp='')
        id, seq, src = es.id, es.seq, es.src

        e1 = jdb.entrList (DBcursor, None, [id])[0]
        expect = [Xref (id,1,1,3,_.t2.id,2,None,1,None,False,False)]
        _.assertEqual (expect, e1._sens[0]._xref)
        _.assertEqual ([],     e1._sens[0]._xrslv)

    def test0003(_):
          # Xref to reading and sense #1.
        e0 = mkentr ('子犬\fこいぬ\f[1][n]puppy [see=うま[2]]', c=99)
        es = submit_ (e0, disp='')
        id, seq, src = es.id, es.seq, es.src
        e1 = jdb.entrList (DBcursor, None, [id])[0]
        expect = [Xref (id,1,1,3,_.t2.id,2,1,None,None,False,False)]
        _.assertEqual (expect, e1._sens[0]._xref)
        _.assertEqual ([],     e1._sens[0]._xrslv)


class Xrslvs (unittest.TestCase):  # Named with plural form because "Xrslv"
    def test0001(_):               #  conflicts objects.Xrslv.
          # An xref to a non-existant target will generate an Xrslv object.
        e0 = mkentr ('犬\fいぬ\f[1][n]dog[see=猫猫]', c=99)
        errs = []
        es = submit_ (e0, disp='')
        id, seq, src = es.id, es.seq, es.src
        e1 = jdb.entrList (DBcursor, None, [id])[0]
        expect = [Xrslv (id,1,1,3,None,'猫猫',None,None,None,None,None)]
        _.assertEqual (expect, e1._sens[0]._xrslv)
        _.assertEqual ([],     e1._sens[0]._xref)


#=============================================================================
# Support functions.

  # Check that an Entr object has expected values.
Ign = object()
def chke (_, entr, id=Ign, c=Ign, s=Ign, d=Ign, u=Ign):
    if id is not Ign: _.assertEqual (id, e.id)
    if c  is not Ign: _.assertEqual (c,  e.src)
    if s  is not Ign: _.assertEqual (s,  e.stat)
    if d  is not Ign: _.assertEqual (d,  e.dfrm)
    if u  is not Ign: _.assertEqual (u,  e.u)

def ishead (id):
        if not id: raise ValueError (id)
          # Returns (python singleton) True if there are no child edits of
          # entry 'id', false if there are (return value False) or if entry
          # 'id' doesn't exist (return value None).
        sql = "SELECT count(e2.id) "\
              "FROM entr e LEFT JOIN entr e2 ON e2.dfrm=e.id "\
              "WHERE e.id=%s GROUP BY e.id"
          # Note: the above sql is used rather then the simpler
          #   select count(*) from entr where entr.dfrm=%s
          # because the former distinguishes between an entries with no
          # children and non-existant entries but the latter doesn't.
        row = db.query1 (DBcursor.connection, sql, [id])
        if not row: return None
          # Due to unique index on entr.dfrm applied in database
          # update 6f3d81, the count value must be 0 or 1; anything else
          # indicates the index is missing or the database is corrupt.
        assert 0 <= row[0] < 2
        return row[0] == 0

def isroot (id):
          # Return True if entry 'id' is a root entry (ie, has NULL .dfrm
          # value), False otherwise.
        row = db.query1 (DBcursor.connection,
                         "SELECT dfrm FROM entr WHERE id=%s", [id])
        return row[0] is None

def isgone (id):
          # Return True if entry 'id' does not exist in the database,
          # False if there is such an entry.
        row = db.query1 (DBcursor.connection,
                         "SELECT count(*) FROM entr WHERE id=%s", [id])
        return row[0] == 0

  # Submit an Entr via submit.submission().  Call this when no errors are
  # expected.  If any occur, a Runtime Exception is raised so they will
  # be noticed without need for caller to explicitly pass and check 'errs'.
def submit_ (entr, **kwds):   # Trailing "_" in function name to avoid
        errs = []             #  name conflict with submit module.
        kwds['errs'] = errs
        e = submit.submission (DBcursor, entr, **kwds)
        if errs or id is None: raise RuntimeError (errs)
          # Update the given Entr instance with the actual db values.
        entr.id, entr.seq, entr.src = e.id, e.seq, e.src
          # Don't commit, transaction will be rolled back automatically.
        return e

  # Submit an Entr via submit.submission().  Call this when errors are
  # expected.  The submission() return values confirmed to be None*3
  # and the value of the 'errs' parameter returned for checking by the
  # caller.
def submitE (_, entr, **kwds):
        errs = []
        kwds['errs'] = errs
        e = submit.submission (DBcursor, entr, **kwds)
          # Don't commit, transaction will be rolled back automatically.
        _.assertEqual (e, None)
        return errs

def setUpModule():
        global DBcursor
        DBcursor = jmdb.DBmanager.use (DBNAME, DBFILE)
        em = jmdb.EntryMaker (DBcursor)
        em = jmdb.EntryMaker (DBcursor)
          # Allow access to EntryMake methods as functions for brevity and
          # compatibilty with earlier revs when they were functions.
        global mkentr, addentr, edentr, addedit,\
          dbread, dbwrite, dbentr, delentr
        mkentr, addentr, edentr, addedit,\
          dbread, dbwrite, dbentr, delentr = em.export()
        db.ex (DBcursor.connection,
               "DELETE FROM entr WHERE src=99; COMMIT")

def tearDownModule():
        #print ("Deleting entries from 'test' corpus", file=sys.stderr)
        DBcursor.connection.rollback()   # In case a test fail left an
                                         #  open aborted transaction.
          # Remove any test entries that we couldn't avoid committing.
        db.ex (DBcursor.connection,
               "DELETE FROM entr WHERE src=99; COMMIT")

def main(): unittest.main()

if __name__ == '__main__': unittest.main()
