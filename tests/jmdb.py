import sys, os, copy, subprocess, hashlib, re, pdb
from jmdictdb import jdb, db

# This module provides support for JMdictDB tests in three forms:
# - A _DBmanager class for loading tests databases.
# - A JEL parser for conveniently constructing Entr objects.
# - A set of methods in EntryMaker for easily creating, editing and
#   submitting entries to a database.
#
#-----------------------------------------------------------------------------
# Define a _DBmanager class and create a single
# global instance of it that test modules may import and use in
# order to share a common database(s) without reloading it for
# each test.  This matters because the "jmtest01" database takes
# some 15+ seconds to reload... way too long to do in each of
# hundreds of tests.
# Sharing a database between tests requires some displine on the
# part of the tests not to make any changes that will affect other
# tests and even with that goal there is some risk it will happen
# inadvertently.  But it seems a fair tradeoff given the complexity
# of database interactions and the difficultly of mocking them as
# well as the situation that many tests are more functional tests
# than pure unit tests.
#
# The main user-callable method of DBmanager is
#
#    .use (dbname, filename)
#
#    dbname -- Name of a test database.
#    filename -- Name of a file containing sql commands needed to
#      create the database.  This is typically produced by running
#      Postgresql's 'pg_dump' command on a prototype test database.
#
# When .use() is called it will check if the database 'dbname' is
# already loaded in the Postgresql server by checking a hash value
# stored in the database when it was first loaded into the database
# server; it's value must match the hash of the filename we want to
# load here.  If it doesn't match we presume the database was loaded
# from some other file, drop the database and reload from our file.
# A failure when loading will result in an exception.

HASH_METHOD = 'sha1'

class _DBmanager():
    def __init__ (self): pass
    def use (self, dbname, filename, force_reload=False):
        if force_reload or not self.is_loaded (dbname, filename):
            print ('Loading database "%s" from %s' % (dbname, filename),
                   file=sys.stderr)
              # load() will raise CalledProcessError if it fails
              # which we let propagate up to our caller.
            loaddb (dbname, filename)
            reset_testsrc (dbname, filename, HASH_METHOD)
        cur = jdb.dbOpen (dbname)
        return cur
    def is_loaded (self, dbname, filename):
        try:
            dbconn = db.connect (dbname)
            rs = db.query (dbconn, "SELECT * FROM testsrc;")
        except db.Error as e:
            if re.search ('database \S+ does not exist', str(e)):
                print ("jmdb: test database %s not found" % dbname,
                       file=sys.stderr)
                return False
            if re.search ('relation \S+ does not exist', str(e)):
                print ("jmdb: no signature table found", file=sys.stderr)
                return False
              # If there is some other error it will probabably reoccur
              # so re-raise it; since we were probably called from a
              # unittest setUpModule() method, the exception will pre-
              # emptively cancel any tests in that module.
            raise
        if len(rs) != 1 or len(rs[0]) != 3:
            print ("jmdb: unexpected signature data", file=sys.stderr)
            return False
          # Code below is commented out in order to ignore the testdb filename
          # and rely only on the hash to identify the expected test database.
          # The motivation is that tests are often run from repository clones
          # causing the filename difference to result in an unnecessary reload.
        #if rs[0][0] != os.path.abspath (filename):
        #    print ("jmdb: filename mismatch", file=sys.stderr)
        #    return False
        if rs[0][2] != hash (HASH_METHOD, filename):
            print ("jmdb: hash mismatch", file=sys.stderr)
            return False
        return True

def loaddb (dbname, filename):
          # Reload a fresh copy of a test database into the postgresql
          # database server.  After loading the database we store the
          # a hash of the load file's contents in a table, "testsrc",
          # in the database.  Subsequent requests to use the database
          # will call .is_loaded() to check the stored hash against the
          # actual file hash to confirm that the requested database is
          # already loaded and need not be reloaded.
          # For safely (to avoid accidently blowing away a production
          # database) we require the test database name to start with
          # "jmtest".
          # The user running this must:
          #  - have "create database" permission on the server.
          #  - have a suitable .pgpass set up that allows access to the
          #    server without needing to interactively supply a password.
        if not dbname.startswith ("jmtest"):
            raise RuntimeError ('jmdb: dbname must start with "jmtest"')
        absfn = os.path.abspath (filename)
          # The PGOPTIONS environment variable used below supresses some
          # Postgresql NOTICE messages that would otherwise appear.
        run ('PGOPTIONS="--client-min-messages=warning" '
               'dropdb --if-exists %s' % dbname)
        run ('createdb %s' % dbname)
          # The -v option tells psql to stop immediately and exit with
          # status 3 if there is an error.  Without it, psql will trudge
          # on, exiting with status 0 unless the error is fatal.
        run ("psql -d %s -f %s -v 'ON_ERROR_STOP=1'" % (dbname, absfn))

def reset_testsrc (dbname, filename, hash_method):
          # Replace any records (normally only one) in table "testsrc"
          # with a new record with the hash value of the file the database
          # was loaded from.  If the table doesn't exist we attempt to
          # create it.
        hashval = hash (hash_method, filename)
        dbconn = db.open (dbname)
        try: db.ex (dbconn, 'DELETE FROM testsrc')
        except db.DatabaseError as e:
            if 'relation "testsrc" does not exist' not in str(e): raise
            dbconn.rollback()   # Clear the existing failed transaction.
            sql = "CREATE TABLE testsrc(filename TEXT,method TEXT,hash TEXT);"
            db.ex (dbconn, sql)
        sql = " INSERT INTO testsrc VALUES(%s,%s,%s)"
        db.ex (dbconn, sql, (filename, hash_method, hashval))
        dbconn.commit()

def hash (method, filename):
        with open (filename, 'rb') as f:
            h = hashlib.new (method, f.read())
        return (h.digest()).hex()

def run (cmd):
      # The "stdout" argument will send stdout to /dev/null; we
      # want to supress all the notice-level messages from psql
      # but errors will be written to stderr so they will be shown.
    subprocess.run (cmd, shell=True, check=True,
                         stdout=subprocess.DEVNULL)

  # Create a single instance of _DBmanager that will be shared by
  # all tests run in a single invocation of a unittest test program.
  # This allows any database(s) loaded to be reused by all tests,
  # regardless of the test class or module they are in.
DBmanager = _DBmanager()

#-----------------------------------------------------------------------------
# This is a JEL parser for use in creating Entr objects for
# tests.  Creating them using JEL is more convenient and concise
# then constructing them by hand from the classes in objects.py.

from jmdictdb import jellex, jelparse
class JelParser:
    def __init__(self, dbcursor=None,
                       src=None, stat=None, unap=None, dfrm=None,
                       debug=False):
          # 'dbcursor' is an open JMdictDB cursor such as returned
          # by jdb.dbOpen() and used when resolving any xrefs in the
          # parsed entry.  It is not required if .parse() wil be
          # called with 'resolve' set to false.
          # 'src', 'stat' and 'unap' are defaults value to use in
          # the Entr objects if values weren't supplid in the JEL
          # text.
          # NOTE: while 'dbcursor' is optional here, jdb.dbOpen()
          # *must* be called prior to executing .parse() since the
          # latter requires the jdb.KW structure to be initialized,
          # which jdb.dbOpen() does.

        self.lexer, tokens = jellex.create_lexer ()
        self.parser = jelparse.create_parser (self.lexer, tokens)
        self.dbcursor,self.src,self.stat,self.unap,self.dfrm,self.debug\
           = dbcursor, src, stat, unap, dfrm, debug
    def parse (self, jeltext, resolve=True, augment=True, dbcursor=None,
               src=None, stat=None, unap=None, dfrm=None):
        jellex.lexreset (self.lexer, jeltext)
          #FIXME? why do we give the jeltext to both the lexer
          # and the parser?  One of the other should be sufficient.
        entr = self.parser.parse (debug=self.debug)
        if not entr.src: entr.src = src or self.src
        if not entr.stat: entr.stat = stat or self.stat
        if entr.unap is None:   # 'unap' may be False
            entr.unap = unap if unap is not None else self.unap
        if not entr.dfrm: entr.dfrm = dfrm or self.dfrm
        if resolve:
            if not dbcursor: dbcursor = self.dbcursor
            if not dbcursor: raise RuntimeError (
                "Xref resolution requested but no database available")
            #FIXME: temporary adjustment for different branches:
              # Use following line for branch 'xrslv".
            #jdb.xresolv (dbcursor, entr)
              # Use following line for branch "master".
            jdb.xresolv (dbcursor, entr)
            if augment:     # Augment the xrefs...
                  # These calls attach additional info to the xrefs that
                  # is needed when when displaying them.  Without it some
                  # information like the target kanji/reading texts is not
                  # available.  Note that since 'entr' was created from
                  # JEL text it is not in the database and consequently
                  # cannot have any reverse xrefs yet; the reverse xref
                  # related calls that are usually used after reading an
                  # entry from the database are commented out for that
                  # reason.
                xrefs = jdb.collect_xrefs (entr)
                #xrers = jdb.collect_xrefs (entr, rev=True)
                jdb.augment_xrefs (dbcursor, xrefs)
                #jdb.augment_xrefs (dbcursor, xrers, rev=True)
                jdb.add_xsens_lists (xrefs)
                jdb.mark_seq_xrefs (dbcursor, xrefs)
        return entr

#=============================================================================
# The methods in this class provide a convenient and concise way to
# create entries and write them to a JMdictDB database for testing purposes.
# A class is used so that DBcursor and JELparser (which will be the same
# for all calls) need not be passed explicitly.

# Please note that the methods below that add entries to the database
# (addentr(), addedit()) do so using the low level jdb.addentr() function
# and will attempt to write whatever they are given.  They do not perform
# any of the validity checking or application-level rule enforcement done
# by submit.submission() such as deleting ancestor entries when an approved
# entry is written.

NoChange = object()

class EntryMaker:
    def __init__ (self, dbcursor):
        self.dbcursor = dbcursor
        self.jelparser = JelParser (dbcursor)

      # Create an Entr object from JEL and optionally add to the database.
    def mkentr (self, jel, q=None, c=99, s=2, a=False, d=None, h=[], dbw=False):
          # We need to pass srcid, stat, unap to .parse because they
          # are used when resolving any xrefs.
        e = self.jelparser.parse (jel, src=c, stat=s, unap=not a, dfrm=d)
        e.seq, e.src, e.stat, e.unap, e.dfrm = q, c, s, not a, d
        if h: e._hist.extend (h)
        if dbw: self.dbwrite (e, c, q)
        return e

      # Same as mkentr() but default database write is True.
    def addentr (self, *args, **kwargs):
        return self.mkentr (*args, dbw=True, **kwargs)

      # "Edit" a copy of an existing entry, 'entr' and optionally add to the
      # database.  Unless overridden by parameter 'd', the new entry's 'dfrm'
      # value is set to 'entr.id' and its 'id set to None.  Other parameters,
      # if given, will set the corresponding attributes.  The new entry object
      # is returned and, if succesfully written to the database, will have
      # its .id and .seq attributes set to the values assigned in the database.
    def edentr (self, entr, q=NoChange, c=NoChange, s=NoChange, a=False,
                      d=NoChange, h=None, dbw=False):
        e = copy.deepcopy (entr)
        if d is NoChange: e.dfrm, e.id = e.id, None
        else: e.dfrm = d
        e.unap = not a
        if q is not NoChange: e.seq = q
        if c is not NoChange: e.src = c
        if s is not NoChange: e.stat = s
        if h: e._hist.append (h)
        if dbw: self.dbwrite (e, c, q)
        return e

      # Same as edentr() but default database write is True.
    def addedit (self, *args, **kwargs):
        return self.edentr (*args, dbw=True, **kwargs)

      # Helper functions for mkentr() and edentr().
    def dbwrite (self, e, c=NoChange, q=NoChange):
        if q is NoChange: q = e.seq
        if c is NoChange: c = e.src
        id,seq,src = jdb.addentr (self.dbcursor, e)
        if not id: raise RuntimeError ("entry not added to database")
        if q is not None and seq!=q:
            raise RuntimeError ("entry has wrong seq# (got %s)"%e.seq)
        if c is not None and src!=c:
            raise RuntimeError ("entry has wrong src# (got %s)"%e.src)
        self.dbcursor.connection.commit()

    def dbread (self, e=None, q=None, c=99):
          # Read the entry row(s) ONLY (not the full entries) for the
          # entry with id# 'e' or set of entries with corpus 'c' and
          # seq# 'q'.  For many tests related to status of or relation-
          # ships between entries only the entr row is needed and this
          # is much more lightweight than reading the full entry.
        if e and q: raise ValueError("call with 'e' or 'q', not both")
        if not e and not q: raise ValueError("'e' or 'q' required")
        if e: whr, args = "id=%s", (e,)
        else: whr, args = "seq=%s AND src=%s", (q, c)
        sql = "SELECT id,src,stat,seq,dfrm,unap"\
              " FROM entr WHERE %s ORDER BY id" % whr
        rs = db.query (self.dbcursor.connection, sql, args)
        return rs

    def dbentr (self, e=None, q=None, c=99, sql='', args=[]):
          # Return a list of fully constructed entries.
          # Aruments in order of priority:
          #   e -- Entry id number.
          #   q, c -- Sequence and corpus numbers.
          #   sql, args -- SQL statement and list of bind arguments.
          # Note that any xrefs in the Entr object will be unaugmented.
        sql_ = "SELECT id FROM entr WHERE src=%s AND seq=%s"
        if e: sql_, args_ = None, [e]
        elif q: args_ = [c, q]
        else: sql_, args_ = sql, args
        entrs = jdb.entrList (self.dbcursor, sql_, args_)
        return entrs    # A list!

    def delentr (self, id):
        self.dbcursor.execute ("DELETE FROM entr WHERE id=%s", (id,))
        self.dbcursor.connection.commit()

      # This is purely a convenience method that allows the caller to easily
      # make this classes' methods available as functions by doing something
      # like:
      #   global mkentr, adentr, ...
      #   em = EntryMaker (...)
      #   mkentr, addentr, ... = em.export()
    def export (self):
        return self.mkentr, self.addentr, self.edentr, self.addedit, \
               self.dbread, self.dbwrite, self.dbentr, self.delentr

#=============================================================================

def main():
        dbmgr = DBmanager.use ('jmtest01', 'data/jmtest01.sql')
        pass

if __name__ == '__main__': main()
