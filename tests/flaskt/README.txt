The tests in this directory exercise the Flask view code.
These tests are at a level in between the end-to-end tests provided 
by the Selenium Web Driver tests in the tests/web/ directory and
the tests of the library code in the tests/tests/ directory.

To run these tests requires the following setup:
- There must be a JMdictDB test user with editor privilege present
  in the "jmsess" (or equiv.) user database.
- The file <jmdictdb-root>/tests/testuser.pw must exist, be readable
  to the user running the tests, should not be world readable, and
  should contain three lines: 1) the test user userid; 2) the test
  user's full name (as present in "jmsess", with matching case; 3)
  the test user's password.
- The file <jmdictdb-root>/web/lib/test.ini must exist.  It contains 
  configuration information as described in the Installtion Guide,
  section XXX but tailored to the tests:
  - Log messages to separate file, eg test.log, so as to not clutter
    up the production log files with test output.)
  - Use "LOG_LEVEL = 60" (one step above "critical") and no LOG_FILTERS
    to suppress all log output by default.  Otherwise the test logfile
    will grow continuously as tests are run.  If logging is useful at
    some point for debugging a test failure, it can be enabled then.
  - It may share the production jmdictdb-pvt.ini file or use a 
    separate one.  The latter is a good idea to avoid giving the 
    test code access to the production 'jmdict" database.  The 
    only database these tests need access to (in addition to the
    "jmsess: database for user login/out) is the "jmtest01"
    database.

To run these tests:

  $ cd  <jmdictdb-root>/tests/
  $ ./runtests.py --dir flaskt 
