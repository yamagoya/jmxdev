# Flask test (support) lib.
# Copyright (c) 2023 Stuart McGraw
# SPDX-License-Identifier: GPL-2.0-or-later

import sys, os, re, urllib, pdb
import logging;  L = logging.getLogger
import lxml, lxml.html
# Additional import(s) below in function(s): serialize(), view()


  # Make a GET (default) or POST request to the Flask app.
  #   url -- The page url, eg: "/srchform.py"
  #   method -- Either 'GET' (default) or 'POST'.
  #   noredir -- Default is to follow redirects.  Use True to not follow.
  #   data -- URL parameters as a dict().
def req (_, url, method='GET', noredir=False, data={}):
        fkwds = {'method':method}
        if not noredir: fkwds['follow_redirects'] = True
        if method == 'GET': param = 'query_string'
        elif method == 'POST': param = 'data'
        if data: fkwds[param] = data
        resp = _.client.open (url, **fkwds)
        return resp

  # Make a POST request to the Flask app.
  #   url -- The page url, eg: "/srchform.py"
  #   noredir -- Default is to follow redirects.  Use True to not follow.
  #   data -- Post data as a dict().
def reqp (_, url, noredir=False, data={}):
        resp = req (_, url, method='POST', noredir=noredir, data=data)
        return resp

  # Parse the HTML byte data returned from a request into an ElementTree
  # instance and return it.
def parse (resp):
        if resp.status_code != 200:
            msg = "HTTP status code: %s, url: %s"%(resp.status_code,resp.url)
            raise OperationError (msg)
        parsed = lxml.html.document_fromstring (resp.data.decode ('utf-8'))
        return parsed

def submit_form (_, form, extra={}):
        '''-------------------------------------------------------------------
        Submit a form with values from 'fields' and 'extra'.
          form -- A parsed "form" element with form.fields items (which are
            the form's input conrols) set to the desired input values.
          extra -- Additional submission data.  This may be a dict or list
            of 2-tuples as described in the Request package documentation.
            This is generally needed  to supply the value of the
            <input type="submit"> element.
        -------------------------------------------------------------------'''
          # We want the lxml submit_form() call to submit the url and
          # retrieve the resulting document using our own get_page() (rather
          # than lxml's default opener function that uses Python's urllib)
          # so that:
          #   1) we can use our established requests.Session to maintain
          #    cookie persistence.
          #   2) we can return the retrieved document in the same form and
          #    processed consistently with other retrieved urls.
          # Submit the form.  We supply any needed hidden input values
          # via the 'extra' argument.
        opener = lambda meth, url, values:\
                     _lxml_submit (_, meth, url, dict(values))
        page = lxml.html.submit_form (form, extra_values=extra,
                                      open_http=opener)
        return page

def _lxml_submit (_, method, url, values):
         # This function is called by lxml.submit_form() indirectly via the
         # _lxml_submit_shim() lambda expression in our submit_form().
           # The lxml.submit_form() doc indicates that GET and POST are
           # the only two methods supported so that's all we'll bother
           # with here.
         if method == 'GET':
             page = parse (req (_, url, data=values))
         elif method == 'POST':
             page = parse (reqp (_, url, data=values))
         else: raise ValueError (method)
         return page

def pkg (entr):
        # Prepare an entry for POSTing to the submit.py view.
        from jmdictdb import jmcgi, serialize
        jmcgi.add_filtered_xrefs ([entr], rem_unap=False)
        serialized = serialize.serialize (entr, compress=True)
        return serialized

def logged_in (page):
          # If logged in, form[0]'s submit button (which is named "loginout")
          # will have a value of "logout" rather than "login".
        xpath = ".//tbody[@class='hlogin']//form//input[@name='loginout']"
        value = page.body.find(xpath).get('value')
        if value == 'logout': return True
        if value == 'login': return False
        raise WrongDataError ("'loginout' button value: %r" % value)

def logged_in_as (page):
          # If logged in, return the user's name extracted from the link
          # to the user.py page.  If not logged in return None..
        xpath = ".//tbody[@class='hlogin']//form/a"
        link = page.body.find(xpath)
        if link is None: return None
        return link.text    # User's full name if logged in or None if not.

def chk_thankyou (_, page):
        chk_title (_, page, "Submission successful")
        xpath = ".//div[@class='jmd-content']/div[@class='item']/table/tr/td"
        cells = page.findall (xpath)
          #FIXME: currently the Thank You page uses <td> elements for the
          # header row so we have an extra row of three items at front of
          # list.
        if cells[0].text == "Corpus": cells = cells[3:]
          #FIXME? the Thank You page supports submission of multiple entries
          # in which case there will multiple rows of (src,seq,id) triples.
          # Since I don't know how to handle that case and am not sure it
          # will occur in practice, we'll just bail.
        if len (cells) != 3:
            raise WrongDataError ("Got %d rows, expected 3" % len(cells), page)
        src = cells[0].text
        href = cells[1].find('a').get('href')
        seq = id_from_url (href, typ="q")
        if not seq: raise WrongDataError ('No seq# found in "%s"' % href, page) 
        href = cells[2].find('a').get('href')
        eid = id_from_url (href, typ="e")
        if not eid: raise WrongDataError ('No id# found in "%s"' % href, page) 
        return eid, seq, src

  # Helper function to do simple verification of received page.
def chk_title (_, et, title, heading=None, prefix="JMdictDB - "):
          # Check that the page title, and optionally the <h2> heading text,
          # is as expected.
          #   et -- Parsed html as an ElementTree instance such as returned
          #     parsed(req()) or parsed(reqp()).
          #   title -- Expected title sans the value of 'prefix'.  Since
          #     'prefix' is the same for most pages, this reduces clutter
          #     in the calls.
          #   heading -- Expected value of the <h2> heading at the top of
          #     each JMdictDB page.
          #   prefix -- Default text appearing at the front of most pages
          #     titles (see 'titles' above).  May be overridden with this
          #     parameter.
        got_title = et.cssselect ("title")[0].text
        _.assertEqual (prefix+title, got_title)
        if heading:
            if heading == "same": heading = title
            got_heading = et.cssselect (".jmd-header h2")[0].text
            _.assertEqual (heading, got_heading)

def extract_errs (_, page, nocheck_title=False):
        if not nocheck_title: chk_title (_, page, "Error")
        xpath = './/div[@class="jmd-content"]'\
                  '//div[@class="item"]/ul/li/span'
        err_elems = page.findall (xpath)
        err_txts = [e.text for e in err_elems]
        errtxt = (".\n".join (err_txts)).strip()
        if not errtxt: raise MissingDataError ("No error text in Error page")
        return errtxt

def id_from_url (url, typ="e"):
          # Extract the int value of an entry id or sequence number from
          # the parameters of a URL text string.  'typ' should be "e" to
          # get the entry id, or "q" to get the sequence number.
          # If no id/seq number is found, None is returned.
        mo = re.search (r'[?&]%s=(\d+)' % typ, url)
        if mo: return int (mo.group(1))
        return ''

def prt (page):
        print (lxml.html.tostring(page).decode('utf-8'))

def view (page):    
          #FIXME? use Python stdlib webbrowser module?
        import subprocess
        fname = "/tmp/jmdict-test.html"
        html = lxml.html.tostring(page).decode('utf-8')
        html = "<!DOCTYPE html>\n" + html
        with open (fname, 'w') as f: print (html, file=f)
        subprocess.run (['firefox', "file://" + fname])

def get_testuser (fname):
        try:
            with open (fname) as f: data = f.read()
        except OSError as e:
            print ("\n%s\n%s" % (str(e).strip(), ErrMsg % fname),
                   file=sys.stderr)
            raise
        testuserid, testusername, testuserpw = data.strip().split('\n')
        return testuserid, testusername, testuserpw

ErrMsg = """\
Unable to read test userid and password.
Expected a file, %s, containing three lines, the first with
the userid of the JMdictDB user (with editor privilege) the tests will
log into JMdictDB as; the second with the user's full name as given in
the "jmsess" (or subsitute) user database. with password for that user; the third
  The file must be readable by the operating system user running
the tests."""

class JMError (RuntimeError):
    def __init__ (self, *args, **kwargs):
        self.args = args;  self.__dict__.update (kwargs)
    def __str__ (self):
        return str ("%s: %s" % (self.__class__.__name__, self.args[0]))
class MissingDataError (JMError): pass
class WrongDataError (JMError): pass
class WrongPageError (JMError): pass
class ErrorPageError (JMError): pass
class LoggedInError (JMError): pass
class NotLoggedInError (JMError): pass
class WrongUserError (JMError): pass
class OperationError (JMError): pass
