#!/usr/bin/env python3
# Copyright (c) 2008 Stuart McGraw
# SPDX-License-Identifier: GPL-2.0-or-later

# Run all or a subset of the JMdictDB tests.
# Run with "--help" option for information on command line
# arguments and options.

import sys, unittest, glob, pdb
import unittest_extensions
  # Adjust sys.path so that the parent directory of this script's
  # directory (which contains the the jmdictdb package we want to
  # test) appears before any system or user library directories
  # which may also contain jmdictdb packages but which we don't
  # want to import.)
_ = sys.path
if '..' not in _[0]: _[:0] = [_[0] + ('/' if _[0] else '') + '..']

def main():
        opts = parse_cmdline()
        args = opts.tests

          # Following raises an exception if wrong jmdictdb pkg was imported.
        chk_import ('jmdictdb', '..')
        suites = []
        testsdir = opts.dir
        if not args:
            pat1 = testsdir+('/' if testsdir else '')+"test_*.py"
            pat2 = testsdir+('/' if testsdir else '')+"T*.py"
            test_files = glob.glob (pat1)
            test_files.extend (glob.glob (pat2))
            for filename in test_files: args.append (filename)

        for testset in args:
            if '/' not in testset:
                testset = testsdir + '.' + testset
            testset = testset.replace('./', '')
            testset = testset.replace('/', '.')
            if testset.endswith ('.py'): testset = testset[:-3]
            testset = testset.lstrip ('.')
            s = unittest.defaultTestLoader.loadTestsFromName (testset)
            if bad_test (s):    # See comments in bad_test() below.
                print ("Cant find or load test: %s"%testset, file=sys.stderr)
                continue
            s.name = testset
            suites.append (s)

        if opts.list: listtests (suites, opts.list.lower()[0])
        else:
            problems = runtests (suites, opts)
            if problems and opts.output:
                print ('Some tests failed, details in file "%s".' % opts.output)

def runtests (suites, opts):
        problems = 0
        if opts.output: outf = open (opts.output, "w")
        else: outf = None
        if opts.verbosity == 1: summary = False
        else: summary = True
        for suite in suites:
            if opts.debug:
                suite.debug(); continue
            runner = unittest_extensions.TextTestRunner (
                stream=sys.stdout, dstream=outf,
                verbosity=opts.verbosity, summary=summary)
            results = runner.run (suite)
            if not results.wasSuccessful(): problems += 1
        return problems

def listtests (obj, sumtyp):
        from collections import defaultdict
        if sumtyp == 't': collect=None
        else: collect = defaultdict (lambda: defaultdict (int))
        scantests (obj, collect)
        if collect is not None:
            for m,v in list(collect.items()):
                if sumtyp == 'm':
                    print ("%s (%d classes, %d tests)" \
                           % (m, len(v), sum (v.values())))
                else:
                    for c,n in list(v.items()):
                        print ("%s.%s (%d tests)" % (m, c, n))

def scantests (obj, collect):
        if isinstance (obj, unittest.TestCase):
            if collect is None: print (obj.id())
            else:
                modnm, clsnm, testnm = obj.id().rsplit ('.', maxsplit=2)
                collect[modnm][clsnm] += 1
        elif isinstance (obj, unittest.TestSuite):
            for s in obj._tests: scantests (s, collect)
        elif hasattr (obj, '__iter__'):
            for s in obj: scantests (s, collect)
        else:
            print ("Unexpected object found: %s" % repr (obj), file=sys.stderr)
            sys.exit (1)

def chk_import (package, relpos):
        ''' Verify that the imported 'jmdictdb' package is from a
            sibling directory of that in which our calling script
            (runtests.py) is located.  This assures we are testing
            code from the development directory and not from any
            system- or user-installed jmdictdb packages. '''
        import os.path as p
          # I've no idea what it means or what to do if there are
          # zero or mutiple items the package's .__path__ which
          # apparently there can be.
        libpath = p.normpath (p.abspath ((__import__ (package)).__path__[0]))
        ourpath =  p.dirname (p.abspath (globals()['__file__']))
        libexpected = p.normpath (p.join (ourpath, relpos, 'jmdictdb'))
        if libpath != libexpected:
             msg = "Wrong library was imported\n" \
                   "  expected '%s'\n"\
                   "  got '%s'"
             raise RuntimeError (msg % (libexpected, libpath))

def bad_test (suite):
          # If unittest.TestLoader.loadTestFromName() can't load a test
          # for some reason (module can't be imported, class or method
          # doesn't exist, etc), it creates a synthetic test that generates
          # a fail when run.  We detect that condition here in order to
          # avoid loading these fake tests into the suite to be run.
          # Returns True if suite has only a Fail test, False otherwise.
        return suite.countTestCases() == 1 \
               and type(suite._tests[0]) is unittest.loader._FailedTest

import argparse
def parse_cmdline():
        p = argparse.ArgumentParser (description="Run the JMdictDB tests.")
        p.add_argument ("tests", nargs='*',
            help="""Test(s) to run.  Each argument has the form
                module[.class[.test]].  If "test" is not given, all the tests
                in the class will be run.  If "class" is not given, all the
                tests in all the classes in the module will be run.  Modules
                are looked for in the directory given by --dir (default is
                "tests".)  If no arguments are given, all modules found
                will be run.
                Modules can also be given as paths to the module file in
                which case shell wildcard patterns may also be used (e.g.,
                `runtrests.py tests/*fmt*.py`).  The --dir option is ignored
                for arguments where the module is given by file path (a
                "/" is present),.""")
        p.add_argument ("--dir", default="tests",
            help="""Look for tests in this directory.  Must be relative
                to the JMdictDB tests/ directory.  "." or "" can be used to
                specify the JMdictDB  tests/ directory itself.  The default
                value is "tests" (i.e., the directory tests/tests/.)""")
        p.add_argument ("-o", "--output", metavar="FILENAME",
            help="""Write details of test failures and errors to
                FILENAME.  If not given details will be written
                to stderr along with the test progress and summary
                information.""")
        p.add_argument ("-v", "--verbosity", type=int, default=1,
            choices=[1,2,3], metavar="LEVEL",
            help="""0: no test progress display, 1: single line
                per module progress display, 2: line per test progress
                display.  Default is 1.""")
        p.add_argument ("-l", "--list", default=None, metavar="WHAT",
            choices=['t','c','m','tests','classes','modules'],
            help="""List tests that would be run, but don't actually
                run them.  WHAT is one of "tests", "classes", or
                "modules".  "tests" lists all test cases.  "classes"
                lists all test classes and the number of test cases
                in each.  "modules" lists all test modules and the
                numbers of classes and test cases in each.
                WHAT can be abbreviated to the first character.""")
        p.add_argument ("-d", "--debug", action="store_true",
            help="""Run tests in debug mode.  In this mode exceptions
                will not be caught by unittest.  If runtests.py is
                also started with the pdb debugger (e.g., with the
                "-mpdb" python option), pdb will be started on the
                exception.  This will occur with *any* exception
                including SkipTest so generally you will want to
                specify a single test in the runtests.py arguments.""")
        args = p.parse_args ()
        return args

if __name__ == '__main__': main()
