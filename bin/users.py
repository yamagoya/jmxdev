#!/usr/bin/env python3

# A simple command line program to manage a JMdictDB sqlite3 users
# database.  This provides a rudimentary command line version of
# the Users web page.
# Run with the --help option for more details.
#
# To create new database and import from PG jmsess database:
#   $ bin/users.py newdb -d web/lib/jmsess.sqlite
#   $ psql jmsess -c "\copy users TO stdout DELIMITER E'\t' CSV" \
#      | bin/users.py -d web/lib/jmsess.sqlite import
#
#FIXME: add option(s) to distinguish between importing from PG jmsess
# database and exported sqlite database.  Currently assumes PG.
#FIXME: need better error handling, catch exceptions from userdb.*.
#FIXME: import: csv values retrieved with doubly quoted text are
# saved to sqlite with excess quotes intact.
#FIXME: import: need option to call with clean=True.

import sys, os, pdb
_=sys.path; _[0]=_[0]+('/' if _[0] else '')+'..'
from jmdictdb import userdb, logger

def main():
        args = parse_cmdline (sys.argv)
        uid = args.user     # For brevity.
        logger.config (level='debug')
        if args.action == 'newdb':
            userdb.newdb (args.database)
            print ("created %s" % args.database)
            return
        dbconn = userdb.dbopen (args.database)
        if args.action == 'list':
            rs = userdb.users (dbconn)
            print_list (rs)
        elif args.action == 'del':
            chk_user (dbconn, uid)
            n = userdb.dele (dbconn, uid)
            assert n in (0, 1)
            if n == 0: err ("failed to delete user '%s'" % uid)
            print ("deleted user '%s'" % uid)
        elif args.action == 'add':
            pw = userdb.get_pw_hash (args.pw, uid)
            n = userdb.add (dbconn, uid, pw, args.name,
                                    args.mail, args.priv, args.enable)
            assert n in (0, 1)
            if n == 0: err ("failed to add user '%s'" % uid)
            print ("added user %s" % uid)
        elif args.action == 'upd':
            chk_user (dbconn, uid)
            pw = userdb.get_pw_hash (args.pw, uid)
            n = userdb.upd (dbconn, uid, pw, args.name,
                                    args.mail, args.priv, args.enable)
            assert n in (0, 1)
            if n == 0: err ("failed to update user '%s'" % uid)
            print ("updated user '%s'" % uid)
        elif args.action == 'export':
            userdb.export_csv (dbconn)
        elif args.action == 'import':
            nread, nloaded = userdb.load_csv_stream (dbconn)
            print ("%s rows read, %s rows loaded" % (nread, nloaded))
        else: assert False, "Programming error"
        dbconn.commit()

def print_list (rs):
          # 'rs' is a recordset received from userdb.users() and is a list
          # of jmdictdb db.DbRow instances, each representing a row from
          # the user table.
        cols = ['userid','fullname','email','priv','disabled']
        t = [cols]
        for r in rs:
            rl = r._tolist();  vals = []
            for n in range (len (cols)):
                vals.append (str (rl[n] or '-'))
            t.append (vals)
        widths = []
        for n,col in enumerate (zip (*t)):  # 'zip(*t)' is transpose.
            widths.append (max ([len(v) for v in col]))
        for r in t:
            fields = []
            for v,w in zip (r, widths):
                fields.append (" %s " % v.ljust(w))
            print (''.join (fields))

def chk_user (dbconn, uid):
        if userdb.chk_user (dbconn, uid): return True
        err ("user '%s' not in database" % uid)

def err (msg): sys.exit ("error: " + msg)

import argparse
def fixed_argparse_fu_error_function (self, message):
          # Argparse's error() method prints a very long single line usage
          # summary and after that (on the same single line!) the actual
          # error message, long past where most users have dismissed the
          # whole thing as gibberish.  Should be submitted to the Daily WTF
          # website.  This is a saner replacement that cuts the usage bit.
        self.exit (2, "error: %s\n" % message)
argparse.ArgumentParser.error = fixed_argparse_fu_error_function
def parse_cmdline (argv):
        p = argparse.ArgumentParser (argv,
            description="Manage JMdictDB users (aka editors).  "
                "This program allows one to add, delete and modify user "
                "details in the users database.")
        p.add_argument ('action', choices=['list','add','upd','del',
                                           'export','import','newdb'],
            help='Action to perform.  '
                '"list": list all users; '
                '"add": add a new user; '
                '"upd": update the settings for an existing user; '
                '"del": delete an existing user; '
                '"export": export to a csv file on stdout; '
                '"import": import a csv file on stdin; '
                '"newdb": (sqlite3 only) create a new database with the '\
                  'filename given.')
        p.add_argument ('user', nargs='?',
             help="User's id (only for actions 'add', 'upd', 'del').")
        p.add_argument ('-d', '--database',default='jmsess',metavar='USER-DB',
            help='Name of the user database, default is "jmsess".')
        p.add_argument ('--pw', nargs='?', default=None, const=...,
            help="Password to be set.  If no value given the program will "
                "prompt for the new pasword.  That is generally a better "
                "choice since command line options are visible to other "
                "users on the system, recorded in shell history, etc.")
        p.add_argument ('-n', '--name', help="Full name.")
        p.add_argument ('-m', '--mail', help="Email address.")
        p.add_argument ('--enable', action='store_true', default=None,
            help="If this option is given, the user account will be enabled.")
        p.add_argument ('--disable', action='store_false', dest='enable',
            help="If this option is given, the user account will be disabled.")
        p.add_argument ('-p', '--priv', choices=['a','e','@'], default=None,
            help='Privilege level for user: '
                '"@": user, no privileges (default for "add"); '
                '"e": editor; '
                '"a": administrator.')
        args = p.parse_args ()
        user_actions = 'add','del','upd'
        if args.action in user_actions and not args.user:
            p.error ("'%s' action requires 'user' argument" % args.action)
        if args.action not in user_actions and args.user:
            p.error ("'user' argument not valid with action '%s'"%args.action)
        return args

main()
