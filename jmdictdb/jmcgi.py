# Copyright (c) 2008-2019 Stuart McGraw
# SPDX-License-Identifier: GPL-2.0-or-later

import sys, os, os.path, re, cgi, html
import urllib.request, urllib.parse, urllib.error
import random, time, http.cookies, datetime, time, copy
from jmdictdb import jdb, config, fmt
from jmdictdb import jinja; from markupsafe import Markup, escape as Escape
from jmdictdb import logger; from jmdictdb.logger import L

  # Path to the config file relative to the current working directory
  # of the web server (which for Apache2/CGI is the web/cgi/ directory.)
CONFIG_FILE = "../lib/config.ini"

def initcgi (cfgfile):
        """
        Does three things:
        1. Fixes any url command line argument so that it is usable by
          the cgi module (see the args() function).
          Currently the only argument accepted is a url used when this
          script run interactively for debugging.  In the future it may
          also accept an option giving the location of a configuration
          file.
        2. Reads the configuration file and optional auth file.  (The
          database authorization details can be set in either the main
          configuration file or in a separate file for only those details
          allowing permissions on the former to be broader than on the
          latter.)
        3. Initializes the Python logging system so log messages will have
          a consistent format. JMdictDB developers should not call the
          logger.L function() until after this function is called.
        """

          # If the .ini filename below has no directory separator in it,
          # it is looked for in a directory on sys.path.  If it does have
          # a separator in it it is treated as a normal relative or
          # absolute path.
        cfg = config.cfgRead (cfgfile)
        logfname = cfg.get ('logging', 'LOG_FILENAME')
        loglevel = cfg.get ('logging', 'LOG_LEVEL')
        filters = parse_cfg_logfilters (
                   cfg.get ('logging', 'LOG_FILTERS'))
          # Logfile path is relative to the config file directory.
          # If 'logfname' is an absolute path, os.path.join() will
          # ignore the preceeding directory so no need for us to check.
          # Access the cfg in dict form because we want a KeyError if
          # the cfg_dir info is unexpectedly not present.
        if logfname:
            logfname = os.path.join (cfg['status']['cfg_dir'], logfname)
        logger.log_config (level=loglevel, ts=True, filename=logfname, filters=filters)
        return cfg

    #FIXME: parseform() and other functions raise standard Python errors
    # (e.g. ValueError) for many detected problems such as a bad entry
    # or sequence number.  Unexpected internal errors of course will
    # also produce Python exceptions.  Currently the cgi scripts catch
    # any exceptions and display them on an error page or via the
    # logger.handler uncaught exception handler.  Showing exceptions
    # that are potentially correctable by a user is ok.  But showing
    # all exceptions is dangerous -- a failure to connect to a database
    # could reveal a url containing a password for example.  Unexpected
    # exceptions should go only into the log file and the error page
    # sgould report something generic ("Oops, something went wrong").
    # Perhaps we should use a separate error class for user-relevant
    # exceptions?

def check_server_status (status_file_dir, ipaddr):
        location = None
        sfd = status_file_dir
        if check_blocked (status_file_dir, ipaddr):
            location = 'status_blocked.html'
        elif os.access (os.path.join (sfd, 'status_maint'), os.F_OK):
            location = 'status_maint.html'
        elif os.access (os.path.join (sfd, 'status_load'), os.F_OK):
            location = 'status_load.html'
        if location:
            print ("Location: %s\n" % ('../' + location))
            sys.exit (0)

def check_blocked (status_file_dir, ipaddr):
        if not ipaddr: return False
        try:
            with open (os.path.join (status_file_dir, 'status_blocked')) as f:
                lines = f.readlines()
        except OSError: return False
        for ln in lines:
              # Comment lines allowed but need no special check since
              # they won't match an IP address.
              # Look at only the first word, allowing ip address to be
              # followed by comment.
            words = ln.strip().split()
            if words and words[0] == ipaddr: return True
        return False

def is_editor (sess):
        """Return True if the 'sess' object (which may be None for a 
        non-logged in user) is for a logged-in editor.  An editor is a
        user with either 'E' (editor) or 'A' (admin) privilege.  A logged
        in user with no privilege will have a sess.priv value of None."""

        if not sess: return False
        return sess.priv in ('E','A')

def get_user (uid, svc, cfg):
        cur = jdb.dbOpenSvc (cfg, svc, session=True, noverchk=True, nokw=True)
        sql = "SELECT * FROM users WHERE userid=%s"
        users = jdb.dbread (cur, sql, (uid,))
          # 'userid' is primary key of users table so we should never
          # receive more than one row.
        assert len(users)<=1, "Too many rows received"
        return users[0] if users else None

def adv_srch_allowed (cfg, sess):
        try: v = (cfg['search']['ENABLE_SQL_SEARCH']).lower()
        except (TypeError, ValueError, KeyError): return False
        if v == 'all': return True
        if v == 'editors' and is_editor (sess): return True
        return False

def form2qs (form):
        """
    Convert a cgi.FieldStorage object back into a query string.
        """
        d = []
        for k in list(form.keys()):
            for v in form.getlist (k):
                d.append ((k, v))
        qs = urllib.parse.urlencode (d)
        return qs

def clean (s):
        if not s: return ''
        if not re.search ('^[0-9A-Za-z_]+$', s):
            raise ValueError ("clean(): Bad string received")
        return s

def str2seq (q):
        # Convert 'q', a string of the form of either 'seq' or
        # 'seq.corp', where 'seq' is a string of digits representing
        # a seq number, and 'corp' is either a string of digits
        # representing a corpus id number or the name of a corpus.
        # The existence of the  corpus, if given, is validated in
        # the KW.SRC dict.  The seq number is only validated as
        # being greater than 0.
        # If sucessful, a 2-tuple of (seq-number, corpus-id) is
        # returned, where 'corpus-id' will be None if the first
        # input string format was given.  Otherwise a ValueError
        # exception is raised.

        KW = jdb.KW
        seq_part, x, corp_part = q.partition ('.')
        try: seq = int (seq_part)
        except (ValueError, TypeError):
            raise ValueError("Invalid seq number '%s'." % (q,))
        if seq <= 0: raise ValueError("Invalid seq number '%s'." % (q,))
        corp = None
        if corp_part:
            corp = corp2id (corp_part)
            if not corp: raise ValueError("Invalid corpus in '%s'." % (q,))
        return seq, corp

def corp2id (c):
        # Convert 'c' which identifies a corpus and is either
        # the corpus id number in integer or string form or
        # the name of a corpus, to the id number of the corpus.
        # The existence id th corpus is validadedin the KW.SRC
        # dict.

        try: c = int (c)
        except (ValueError, TypeError): pass
        try: corpid = jdb.KW.SRC[c].id
        except KeyError: return None
        return corpid

def str2eid (e):
        n = int (e)
        if n <= 0: raise ValueError(e)
        return n

def get_entrs (dbh, elist, qlist, errs, active=None, corpus=None):
        # Retrieve a set of Entr objects from the database, specified
        # by their entry id and/or seq numbers.
        #
        # dbh -- Open dbapi cursor to the current database.
        # elist -- List of id numbers of entries to get.  Each number
        #       may by either a integer or a string.
        # qlist -- List of seq numbers of entries to get.  Each seq
        #       number may be an integer or a string.  If the latter
        #       it may be followed by a period, and a corpus identifier
        #       which is either the corpus id number or the corpus name.
        # errs -- Must be a list (or other append()able object) to
        #       which any error messages will be appended.  The error
        #       messages may contain user data and should thus be
        #       escaped before being used in a web page.
        # active -- If 1, only active/approved or new/(unapproved)
        #       entries will be retrieved.
        #       If 2, at most one entry will be returned for each seq number
        #       in the results and that entry will be the most recently edited
        #       (chronologically based on history records) entry if one exists
        #       of the approved active entry.
        #       If active is any other value or not present, all entries
        #       meeting the entry-id, seq, or seq-corpus criteria will be
        #       retrieved.
        # corpus -- If not none, this is a corpus id number or name
        #       and will apply to any seq numbers without an explicit
        #       corpus given with the number.
        #
        # If the same entry is specified more than once in 'elist' and/or
        # 'qlist' ir will only occur once in the returned object list.
        # Objects in the returned list are in no particular order.

        eargs = []; qargs = []; xargs = []; whr = [];  corpid = None
        if corpus is not None:
            corpid = corp2id (corpus)
            if corpid is None:
                errs.append ("Bad corpus parameter: %s" % corpus)
                return []
        for x in (elist or []):
            try: eargs.append (str2eid (str(x)))
            except ValueError:
                errs.append ("Bad url parameter received: " + x)
        if eargs: whr.append ("id IN (" + ','.join(['%s']*len(eargs)) + ")")

        for x in (qlist or []):
            try: args = list (str2seq (str(x)))
            except ValueError:
                errs.append ("Bad parameter received: " + x)
            else:
                if corpus and not args[1]: args[1] = corpid
                if args[1]:
                    whr.append ("(seq=%s AND src=%s)"); qargs.extend (args)
                else:
                    whr.append ("seq=%s"); qargs.append (args[0])
        if not whr: errs.append ("No valid entry or seq numbers given.")
        if errs: return None
        whr2 = ''; distinct = ''; hjoin = ''; order = ''
        try: active = int (active)
        except (ValueError, TypeError): pass
        if active == 1:
              # Following will restrict returned rows to active/approved
              # (stat=A and not unap) or new (dfrm is NULL), that is, the
              # result set will not include any stat=D or stat=R results.
            whr2 = " AND stat=%s AND (NOT unap OR dfrm IS NULL)"
            xargs.append (jdb.KW.STAT['A'].id)
        elif active == 2:
              # Restrict returned rows to active (no stat=D or stat=R results)
              # and most recent edit as determined by the history records (if any).
              # In no event will more than one entry per seq number be returned.
              # Note that this will necessarily return the edit from only one
              # branch when multiple branches exist which may result in surprise
              # for a user when the returned entry shows no signs of a recent
              # edit known to have been made.
              # Example of generated sql:
              # SELECT DISTINCT ON (e.seq) e.id FROM entr e LEFT JOIN hist h ON h.entr=e.id
              #  WHERE e.seq=2626330 and e.stat=2 ORDER BY e.seq,h.dt DESC NULLS LAST;
            whr2 = " AND e.stat=%s"
            xargs.append (jdb.KW.STAT['A'].id)
            distinct = " DISTINCT ON (e.seq)"
            hjoin = " LEFT JOIN hist h ON h.entr=e.id"
              # "NULLS LAST" is needed below because some entries (e.g., entries
              # imported when JMdictDB is first initialized and never edited)
              # may not have history records which will result in 'dt' values of
              # NULL; we want those entries last.
            order = " ORDER BY e.seq,h.dt DESC NULLS LAST"
        sql = "SELECT" + distinct + " e.id FROM entr e " \
                 + hjoin + " WHERE (" + " OR ".join (whr) + ")" + whr2 + order
        entries, raw = jdb.entrList (dbh, sql, eargs+qargs+xargs, ret_tuple=True)
        if entries:
            jdb.augment_xrefs (dbh, raw['xref'])
            jdb.augment_xrefs (dbh, raw['xrer'], rev=1)
            jdb.add_xsens_lists (raw['xref'])
            jdb.mark_seq_xrefs (dbh, raw['xref'])
        return entries

def htmlprep (entries):
        """\
        Prepare a list of entries for display with an html template
        by adding some additional information that is inconvenient to
        generate from within a template."""

        add_p_flag (entries)
        add_restr_summary (entries)
        add_stag_summary (entries)
        add_audio_flag (entries)
        add_editable_flag (entries)
        add_unreslvd_flag (entries)
        add_pos_flag (entries)
        linkify_hists (entries)
        rev_hists (entries)

def linkify_hists (entries):
        """\
        For every Hist object in each Entr object in the list 'entries',
        replace the .notes (aka comments) and .refs text strings with
        values that have had all substrings that look like urls replaced
        with actual html links."""

        for e in entries:
            for h in e._hist:
                h.notes = linkify (h.notes)
                h.refs = linkify (h.refs)

def add_p_flag (entrs):
        # Add a supplemantary attribute to each entr object in
        # list 'entrs', that has a boolean value indicating if
        # any of its readings or kanji meet wwwjdic's criteria
        # for "P" status (have a freq tag of "ichi1", "gai1",
        # "spec1", or "news1").

        for e in entrs:
            if jdb.is_p (e): e.IS_P = True
            else: e.IS_P = False

def add_restr_summary (entries):
        # This adds an _RESTR attribute to each reading of each entry
        # that has a restr list.  The ._RESTR attribute value is a list
        # of text strings giving the kanji that *are* allowed with the
        # reading.  Recall that the database (and entry object created
        # therefrom) stores the *disallowed* reading/kanji combinations
        # but one generally wants to display the *allowed* combinations.
        #
        # Also add a HAS_RESTR boolean flag to the entry if there are
        # _restr items on any reading.

        for e in entries:
            if not hasattr (e, '_rdng') or not hasattr (e, '_kanj'): continue
            for r in e._rdng:
                if not hasattr (r, '_restr'): continue
                rt = fmt.restrtxts (r._restr, e._kanj, '_restr')
                if rt: r._RESTR = rt
                e.HAS_RESTR = 1

def add_stag_summary (entries):
        # This adds a STAG attribute to each sense that has any
        # stagr or stagk restrictions.  .STAG is set to a single
        # list that contains the kana or kanji texts strings that
        # are allowed for the sense under the restrictions.

        for e in entries:
            for s in getattr (e, '_sens', []):
                rt = []
                if getattr (s, '_stagr', None):
                    rt.extend (fmt.restrtxts (s._stagr, e._rdng, '_stagr'))
                if getattr (s, '_stagk', None):
                    rt.extend (fmt.restrtxts (s._stagk, e._kanj, '_stagk'))
                if rt:
                    s._STAG = rt

def add_audio_flag (entries):
        # The display template shows audio records at the entry level
        # rather than the reading level, so we set a HAS_AUDIO flag on
        # entries that have audio records so that the template need not
        # sear4ch all readings when deciding if it should show the audio
        # block.
        # [Since the display template processes readings prior to displaying
        # audio records, perhaps the template should set its own global
        # variable when interating the readings, and use that when showing
        # an audio block.  That would eliminate the need for this function.]

        for e in entries:
            if getattr (e, '_snd', None):
                e.HAS_AUDIO = 1;  continue
            for r in getattr (e, '_rdng', []):
                if getattr (r, '_snd', None):
                    e.HAS_AUDIO = 1
                    break

def add_editable_flag (entries):
        # This is a convenience function to avoid embedding this logic
        # in the page templates.  This sets a boolean EDITABLE flag on
        # each entry that says whether or not an "Edit" button should
        # be shown for the entry.  All unapproved entries, and approved
        # active or deleted entries are editable.  Rejected entries aren't.

        KW = jdb.KW
        for e in entries:
            e.EDITABLE = e.unap or (e.stat == KW.STAT['A'].id)\
                                or (e.stat == KW.STAT['D'].id)

def add_unreslvd_flag (entries):
        # This is a convenience function to avoid embedding this logic
        # in the page templates.  This sets a boolean UNRESLVD flag on
        # each entry that says whether or not it has any senses that
        # have unresolved xrefs in its '_xunr' list.

        KW = jdb.KW
        for e in entries:
            e.UNRESLVD = False
            for s in e._sens:
                if len (getattr (s, '_xunr', [])) > 0:
                    e.UNRESLVD = True

def add_pos_flag (entries):
        # This is a convenience function to avoid embedding this logic
        # in the page templates.  This sets a boolean POS flag on
        # each entry if any senses in the entry have a part-of-speech
        # (pos) tag that is conjugatable.  A POS tag is conjugatable
        # if its id number is an id number in jdb.KW.COPOS.  jdb.KW.COPOS
        # in turn is populated from database view 'vcopos' which are those
        # rows in kwpos which are also referenced in table 'copos' (that
        # identifies them as conjugatable.)  See also the comments for
        # view 'vcopos' in pg/conj.sql.
        # The .POS attribute set by this function is used to determine
        # if a "Conjugations" link should be shown for the entry.
        # We exclude nouns and na-adjectives even though the conjugator
        # will happily conjugate them as だ-predicates.

        KW = jdb.KW
        conjugatable_poslist = [x.id for x in jdb.KW.recs('COPOS')
                                if x.kw not in ('n', 'adj-na')]
        for e in entries:
            e.POS = False
            for s in e._sens:
                for p in s._pos:
                    if p.kw in conjugatable_poslist:
                        e.POS = True;  break

def rev_hists (entries):
        # Reverse the order of history items in each entry so that the
        # most recent will appear first and the oldest last.

        for e in entries:
            if e._hist: e._hist = list (reversed (e._hist))

def add_filtered_xrefs_old (entries, rem_unap=False):
        # Generate substitute _xref and _xrer lists and put them in
        # sense attribute .XREF and .XRER.  These lists are copies of
        # ._xref and ._xrer but references to deleted or rejected
        # entries are removed.  Additionally, if 'rem_unap' is true,
        # references to unapproved entries are also removed *if*
        # the current entry is approved.
        # The xrefs in ._xref and ._xrer must be augmented xrefs (i.e.
        # have had jdb.augment_xrefs() called on the them.)
        #
        # FIXME: have considered not displaying reverse xref if an
        #  identical forward xref (to same entr/sens) exists.  If
        #  we want to do that, this is the place.

        cond = lambda e,x: (e.unap or not x.TARG.unap or not rem_unap) \
                            and x.TARG.stat==jdb.KW.STAT['A'].id
        for e in entries:
            for s in e._sens:
                s.XREF = [x for x in s._xref if cond (e, x)]
                s.XRER = [x for x in s._xrer if cond (e, x)]

def add_filtered_xrefs (entries, rem_unap=False):
        # Works like add_filtered_xrefs_old() above except:
        # Put all xrefs, both forward (from list s._xref) and reverse
        # from list s._xrer), into s.XREF and creates an additional
        # attribute on the s.XREF objects, .direc, that indicates whether
        # the xref is a "from", "to" or "bidirectional" xref.  If the
        # same xref occurs in both the s._xref and s_xrer lists, it is
        # only added once and the .direc attribute set to "bidirectional".
        # This allows an entr display app (such as entr.py/entr.jinja) to
        # display an icon for the xref direction.  This was suggested
        # on the Edict maillist by Jean-Luc Leger, 2010-07-29, Subject:
        # "Re: Database testing - call for testers - more comments"

        cond = lambda e,x: (e.unap or not x.TARG.unap or not rem_unap) \
                            and x.TARG.stat==jdb.KW.STAT['A'].id
        def setdir (x, dir):
            x.direc = dir
            return x

        FWD = 1;  BIDIR = 0;  REV = -1  # Direction of an xref.
        for e in entries:
            for s in e._sens:
                  # Add all (non-excluded) forward xrefs to the xref list,
                  # s.XREF.  Some of these may be bi-directional (have
                  # xrefs on the target that refer back to us) but we will
                  # indentify those later to fixup the FWD flag to BIDIR.
                s.XREF = [setdir (copy.deepcopy(x), FWD) for x in s._xref if cond (e, x)]
                  # Make dict of all the fwd xrefs.
                fwdrefs = {(x.entr,x.sens,x.xentr,x.xsens):x for x in s.XREF}
                for x in s._xrer:
                    if not cond (e, x): continue
                    x = copy.deepcopy (x)
                      # Because we will display the reverse xref with the
                      # same code that displays forward xrefs (that is, it
                      # creates a link to the entry using x.xentr and x.xsens),
                      # swap the (entr,sens) and (xentr,xsens) values so that
                      # when (xentr,xsens) are used, they'll actually be
                      # the entr,sens values which identify the entry the
                      # the reverse xref is on, which is what we want.
                    x.entr,x.sens,x.xentr,x.xsens = x.xentr,x.xsens,x.entr,x.sens
                      # Is this reverse xref the same as a forward xref?
                    if (x.entr,x.sens,x.xentr,x.xsens) in fwdrefs:
                          # If so, change the forward xref's 'direc' attribute
                          # from FWD to BIDIR.
                        setdir (fwdrefs[(x.entr,x.sens,x.xentr,x.xsens)], BIDIR)
                      # Otherwise (our xref is not bi-directional), add us to
                      # the xref list as a reverse xref.
                    else: s.XREF.append (setdir (x, REV))

def add_encodings (entries, gahoh_url=None):
        # Add encoding info which is displayed when presenting kanjidic
        # (or similar single-character) entries.

        for e in entries:
            if not hasattr (e, 'chr') or not e.chr: continue
            c = e.chr; c.enc = {}
            c.enc['uni'] = hex (ord (c.chr)).upper()[2:]
            c.enc['unid'] = ord (c.chr)
            c.enc['uurl'] = 'http://www.unicode.org/cgi-bin/GetUnihanData.pl?codepoint=' + c.enc['uni']
            c.enc['utf8'] = ' '.join(["%0.2X"%x for x in c.chr.encode('utf-8')])
            try:
                iso2022jp = c.chr.encode ('iso2022-jp')
                c.enc['iso2022jp'] = ' '.join(["%0.2X"%x for x in iso2022jp])
                c.enc['jis'] = ''.join(["%0.2X"%x for x in iso2022jp[3:5]])
            except UnicodeEncodeError: c.enc['iso2022jp'] = c.enc['jis'] = '\u3000'
            try: c.enc['sjis'] = ''.join(["%0.2X"%x for x in c.chr.encode('sjis')])
            except UnicodeEncodeError: c.enc['sjis'] = '\u3000'
            try: c.enc['eucjp'] = ''.join(["%0.2X"%x for x in c.chr.encode('euc-jp')])
            except UnicodeEncodeError: c.enc['eucjp'] = '\u3000 '

def linkify (s, newpage=True):
        """Convert all text substrings in 's' that look like http,
        https or ftp url's into html <a href=...> links.  Since the
        return value will be further processed by the Jinja2 template
        engine which normally html-escapes text prior to output, we
        manually html-escape the non-url parts of 's' here and return
        the results as a markupsafe string that Jinja2 will not escape
        again.  For markupsafe doc see:
          https://markupsafe.palletsprojects.com/.
        If 'newpage' is True, the generated links will open the link
        in a new browser page/tab rather than the same page."""

        if not s: return s
          # The following regex used to identify urls and is based on:
          #    https://www.regextester.com/96504
          # modified to require a url scheme prefix (e.g. "http://") 
        urlpat = r'''(?:https?|ftp):\/\/(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?'''
          # Markup is the markupsafe.Markup() function, Escape is the
          # markupsafe.escape() function.  The former returns a str
          # subtype that will not be subject to the html-escaping that
          # jinja2 does on normal strings.  That latter returns a
          # different str subtype that is html-escaped and will not
          # be escaped again by jinja2.
        t = Markup('');  prev_end = 0;  end = len(s)
          # Iterate through the urls in 's'.  Each 'mo' is a "re"
          # module match object for one url.
        for mo in re.finditer (urlpat, s):
            start, end = mo.start(), mo.end()
              # The url text in 's' is at s[start:end].  'prev_end' is
              # the ending index of the url found before this one.
              # If there is any non-url text in front of the url,
              # escape it and append it to the results, 't'.
            if start > prev_end: t += Escape (s[prev_end:start])
              # Convert the url text into an html link.
            url = mo.group(0)
            targ = ' target="_blank"' if newpage else ''
              #FIXME? we url-decode the displayed link text since the encoded
              # form with many %xx values is often unreadable.  But this may be
              # confusing since it may lead one to look in the database for
              # for the decoded value when it is actually the encoded value
              # that is stored.
            link = '<a href="%s"%s>%s</a>' \
                   % (url, targ, urllib.parse.unquote(url))
              # Wrap the link with Markup() so that it won't be escaped
              # by jinja2 later.
            t += Markup (link)
              # Update 'prev_end' for next iteration.
            prev_end = end
          # Look for any non-url text after the last url, escape it,
          # and append to results, 't'.
        if prev_end < len(s):
            t +=  Escape (s[prev_end:len(s)])
        return t

def parse_cfg_logfilters (s):
        result = []
        for ln in s.splitlines():
            ln = ln.strip ()
            if not ln: continue
            result.append (ln)
        return result
