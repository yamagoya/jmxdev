#!/usr/bin/env python3
# Copyright (c) 2019,2023 Stuart McGraw
# SPDX-License-Identifier: GPL-2.0-or-later

# In the database entr table rows have three attributes that
# support entry editing:
#   dfrm -- Contains the entr.id number of the entry that the
#       current entry was derived from (parent entry).
#   unap -- Indicates the current entry is not "approved"
#   stat -- Has value corresponding to one of the kwstat kw's:
#       2 -- Active entry
#       4 -- Deleted entry
#       6 -- Rejected entry
#
# Basics:
# 1. An entry object is represented in the database by a row in table
#    "entr" and zero or more rows in related tables (e.g., "kanj" for
#    the kanji associated with the entry, "hist" for the history of
#    of changes made to the entry, etc.)
# 2. All edits to entries result in creating new entry objects in the
#    database; the rows representing existing objects are never updated
#    in place.
# 3. When an existing entry is edited and submitted as a new unapproved
#    edit, an entry object is added to the database that represents the
#    object as edited, but leaves the original entry object which provided
#    the source for the edit (the parent entry) in the database as well.
# 4. When a new object is created to represent an edit, the history
#    records of the parent entry are copied to it and added
#    to the new history record recording the edit itself.
# 5. The added edited entry contains the id number of the entry it was
#    edited from (its "parent" entry) in attribute ".dfrm" unless it is
#    a new entry in which case its .dfrm attribute is NULL (or in Python,
#    None).
# 6. The set of an entry and its edits form a linear "edit chain" with
#    the .dfrm attribute of a newer entry pointing to the next older
#    one.  The oldest (root) entry has a .dfrm value of NULL (in the
#    database, None in Python).
# 7. Only the most recently edited entry (called the "head" entry) can
#    be further edited.  This is enforced in the database by a unique
#    constraint on the entr.dfrm column.  (The web UI allows submitting
#    edits of older entries by rebasing them to the most recently edited
#    entry before submission.)
# 8. When an entry is approved, a new entry is created as an edit of the
#    unapproved entry with its .unap ("unapproved") attribute set to False,
#    its .dfrm value set to NULL.  As mentioned, the change history of the
#    parent is copied to the new entry as well.  The  parent entry (as well
#    as any parent's parent, and so on, recursively) are erased from the
#    database leaving only the approved entry present in the database.
# 9. When an entry is rejected, the head entry of the edit chain is copied
#    and becomes the parent of the new entry.  The new entry's .stat
#    attribute is changed to '6', its .dfrm set to NULL, and its .unap
#    value set to False (approved; there are no unapproved, rejected entries.)
#    The entry rejected (assuming it's not the head entry) and any entries
#    on the edit chain up to but not including the head entry, are erased
#    from the database, leaving the approved, rejected entry and the entries
#    on the edit chain up to the entry selected for rejection, in the
#    database.
# 10. When an entry is "deleted", it is treated much like a edit to the
#    the entry's content: a new edit entry is created with the original
#    entry as parent, but the edited entry has a .stat value of 4.  Approval
#    is like approval of any other edit.  The approved "deleted" entry
#    remains in the database and any parents are erased.
# 11. Note: although the words "older" and "newer" are used here, they
#    don't necessarily have temporal meanings: older and newer are defined
#    stictly by the entry's position in the .dfrm chain.
# 12. submission()'s role to write the entry it is given to the database
#    making only the minimal changes to ensure the integrity of the parts
#    of the entry the submitter is not allowed to change: the history from
#    its claimed parent entry, the timestamp and userid in the current
#    history record, etc.  Any other changes are the responsibility of
#    of the submitter.
# 13. Item #12 is the justification for the different handling of edit
#    (disp='') and reject (disp='r') entries: the semantics of rejecting
#    an entry requires it to be rebased to the edit chain head, thus that
#    action is performed here.  Adding a non-head edit is a UI choice, it
#    could rebase the edit to the the head (as views/edconf.py currently
#    does) or report an error and require the user to edit the head entry.
#    Thus that action is left up to the caller.
#
# Concurrency:
# ------------
# Although most user-facing apps will check and warn of problematic
# condidtions before submission, the database can change states between
# those checks and when the entry is actually submitted.  Another user may
# submit other edits of the same entry resulting in a parent that is no
# longer a head entry.  An editor may approve or reject edits resulting
# in the disappearance of the edited entry's parent.  These situations
# are detected in the last resort case by constraints in the database that
# reject invalid submission.  Database operations take place inside a
# transaction so that an error results in no changes at all and the
# operation can be redone by the user with respect to the new state.
#
#FIXME: should dump the method of returning errors in an 'errs'
# list and use exceptions instead.

import sys, pdb
from jmdictdb import logger; from jmdictdb.logger import L
from jmdictdb import jdb
from jmdictdb import db     # To get psycopg2 exceptions.

  #FIXME: Temporary hack...
Svc, Sid = None, None

def submission (dbh, entr, disp, errs, is_editor=False, userid=None):
        # Add a changed entry, 'entr', to the jmdictdb database accessed
        # by the open DBAPI cursor, 'dbh'.
        #
        # dbh -- An open DBAPI cursor
        # entr -- A populated Entr object that defines the entry to
        #   be added.  See below for description of how some of its
        #   attributes affect the submission.
        # disp -- Disposition, one of three string values:
        #   '' -- Submit as normal user.
        #   'a' -- Approve this submission.
        #   'r' -- Reject this submission.
        # errs -- A list to which an error messages will be appended.
        #   Note that if the error message contains html it should be
        #   wrapped in jmcgi.Markup() to prevent it from being escaped
        #   in the template.  Conversely, error messages that contain
        #   text from user input should NOT be so wrapped since they
        #   must be escaped in the template.
        # is_editor -- True is this submission is being performed by
        #   a logged in editor.  Approved or Rejected dispositions will
        #   fail if this is false.  Its value may be conveniently
        #   obtained from jmcgi.is_editor().  False if a normal user.
        # userid -- The userid if submitter is logged in editor or
        #   None if not.
        #
        # Returns:
        #   If there are no errors, an objects.Entr() object that may have
        #   the same identity as 'entr' or *may be a distinct object* is
        #   returned.  It will have its '.id', '.unap' and possibly other
        #   attributes (eg, '.seq', ) set to the values used in the database.
        #   It will have the set of history records from 'entr' plus an
        #   additional one representing the current submission.
        #   IMPORTANT: the submitted entry has not been commited, the caller
        #   is expected to do that.
        #   If an error occured, error messages will be appended to 'errs'
        #   and None returned.
        #
        # Note: currently, for regular submissions, the given 'entr' object
        # is updated and returned.  For "reject and "delete" submissions,
        # a new objects.Entr object is returned that represents 'entr's
        # parent object in the database.  The parent object is used for the
        # submission because we want to ignore any changes the submitter
        # may have made to 'entr'.
        #
        # Note that we never modify existing database entries other
        # than to sometimes completetly erase them.  Submissions
        # of all three types (submit, approve, reject) *always*
        # result in the creation of a new entry object in the database.
        # The new entry will be created by writing 'entr' to the
        # database.  The following attributes in 'entr' are relevant:
        #
        #   entr.dfrm -- If None, this is a new submission.  Otherwise,
        #       it must be the id number of the entry this submission
        #       is an edit of.
        #   entr.stat -- Must be consistent with changes requested. In
        #       particular, if it is 4 (Delete), changes made in 'entr'
        #       will be ignored, and a copy of the parent entry will be
        #       submitted with stat D.
        #   entr.src -- Required to be set, new entry will copy.
        #       # FIXME: prohibit non-editors from making src
        #       #  different than parent?
        #   entr.seq -- If set, will be copied.  If not set, submission
        #       will get a new seq number but this untested and very
        #       likely to break something.
        #       # FIXME: prohibit non-editors from making seq number
        #       #  different than parent, or non-null if no parent?
        #   entr.hist -- The last hist item on the entry will supply
        #       the comment, email and name fields to newly constructed
        #       comment that will replace it in the database.  The time-
        #       stamp and diff are regenerated and the userid field is
        #       set from our userid parameter.
        #       # FIXME: should pass history record explicity so that
        #       #  we can be sure if the caller is or is not supplying
        #       #  one.  That will make it easier to use this function
        #       #  from other programs.
        # The following entry attributes need not be set:
        #   entr.id -- Ignored (reset to None).
        #   entr.unap -- Ignored (reset based on 'disp').
        # Additionally, if 'is_editor' is false, the rdng._freq and
        # kanj._freq items will be copied from the parent entr rather
        # than using the ones supplied  on 'entr'.  See jdb.copy_freqs()
        # for details about how the copy works when the rdng's or kanj's
        # differ between the parent and 'entr'.

        KW = jdb.KW
        global Svc, Sid     #FIXME: temporary hack...
        L('submit.submission').info(("disp=%r, is_editor=%r, userid=%r, entry id=%r,\n" + " "*24 + "parent=%r, stat=%r, unap=%r, seq=%r, src=%r")
          % (disp, is_editor, userid, entr.id, entr.dfrm, entr.stat,
             entr.unap, entr.seq, entr.src))
        L('submit.submission').info("entry text: %s %s"
          % ((';'.join (k.txt for k in entr._kanj)),
             (';'.join (r.txt for r in entr._rdng))))
        L('submit.submission').debug("seqset: %s"
          % logseq (dbh, entr.seq, entr.src))

        entr, isparent, edchain, opid, root_unap \
            = prepare (dbh, entr, disp, errs, is_editor, userid)
        if errs: return None
        try:
              #FIXME? functions called below can raise a psycopg2
              # TransactionRollbackError if there is a serialization
              # error resulting from a concurrent update.  We currently
              # trap it and report it as an error but it could be retried
              # and would likely succeed.  Other exceptions
              # (IntegrityError, etc) are hard errors, no point retrying.
            if not disp:
                added = _submit (dbh, entr)
            elif disp == "a":
                added = _approve (dbh, entr, (edchain+[None])[0])
            elif disp == "r":
                added = _reject (dbh, entr, opid)
            else:
                L('submit.submission').debug("bad 'disp' parameter: %r" % disp)
                errs.append ("Bad 'disp' parameter: %r" % disp)
        except db.IntegrityError as e:
            c = None
            if 'entr_src_seq_idx' in str(e):
                errs.append ("[seq_vio] "+Noentr_msg)
            elif 'entr_dfrm_fkey' in str(e):
                errs.append ("[dfrmfk_vio] "+Noentr_msg)
            elif 'entr_dfrm_idx'  in str(e):
                msg = "Other edits have been made to the entry, "\
                      "please edit the latest version"
                errs.append ("[dfrmuniq_vio] "+msg)
            else: raise
            L('submit.submission').debug("constraint violation: %s" % c)
            dbh.connection.rollback()
        L('submit.submission').debug("seqset: %s"
                                     % logseq (dbh, entr.seq, entr.src))
        if errs:
            L('submit.submission').debug("Entry not submitted due to errors:")
            for e in errs: L('submit.submission').debug('  '+e)
            return None
          # Note that changes have not been committed yet, caller is
          # expected to do that.
        return entr

def prepare (dbh, entr, disp, errs, is_editor=False, userid=None):
          # Prepare an entry, possibly received and deserialized from
          # an untrusted source (ie, from the internet) for writing to
          # the database.  Specifically, this function makes sure that
          # any parts of the entry that a regular user or editor is not
          # supposed to change (eg, time stamp in the history entry),
          # are reverted to their correct values.
          # It also checks for illegal state in the entry but only for
          # those things that could result from user input in the web
          # Edit page, or things that the database integrity checks
          # can't catch.  For things that can't be generated by the
          # Edit form (ie, would require a constructed entry) and which
          # will be caught by the database, we mostly leave to the data-
          # baseto do so.  These checks are intended to allow an earlier
          # warning to the user about input errors than waiting until
          # the entry is actually rejected by the database.
        KW = jdb.KW
        lgid = 'submit.prepare'     # Used as id in log messages.
        err = None, None, [], None, None      # Early error return value.
        assert entr.stat in [KW.STAT[chr].id for chr in "ADR"]
          # Submissions, approvals and rejections will always produce a
          # new db entry object so nuke any id number.
        entr.id = None
        assert disp in ('', 'a', 'r')
        entr.unap = not disp	# disp=='' means submit as unapproved.
        pentr, use_parent = None, False
        edchain, opid, root_unap = [], None, None

        if disp in ('a', 'r') and not is_editor:
            msg = "Only logged in editors can approve or reject entries"
            errs.append (msg);  return err
        if not entr.dfrm:       # This is a submission of a new entry.
            if not is_editor:
                entr.seq = None # Force addentr() to assign seq number.
        else:   # Modification of existing entry.
            edchain, root_unap = get_edchain (dbh, entr.dfrm)
            L(lgid).debug("get_edchain:%r"%((edchain,root_unap),))
            if edchain is None:
                errs.append ("[noroot] "+Noentr_msg);  return err

            opid = pid = entr.dfrm     # id# of entry that 'entr' is an edit of.
            if disp == 'r':
                  # This is a rejection.
                if entr.dfrm == edchain[0] and not root_unap:
                    L('submit.reject').debug("IsApprovedError")
                    errs.append ("You can only reject unapproved entries.")
                    return err
                entr.stat = KW.STAT['R'].id
                  # We want the effective parent entry that will supply the
                  # history records to the final rejected entry to be the head
                  # (latest) edit, not the actual parent of the received edited
                  # entry.  The latter serves only to identify the oldest entry
                  # to delete (along with its children entries) up to the head
                  # entry when performing the rejection.
                pid = edchain[-1]      # Effective parent entry's id.
                entr.dfrm = pid        # jdb.add_hist() requires the edit
                                       #  entry to be child of claimed parent.
              # Get the parent entry.
            L('submit.prepare').debug("reading parent entry %d" % pid)
            pentr, raw = jdb.entrList (dbh, None, [pid], ret_tuple=True)
              # We are inside a transaction and looking at a consistent
              # database snapshot; if the parent entry is gone now it was
              # gone when get_edchain() was called above and would have
              # produced a "noroot" error then.
            assert len (pentr) == 1
            pentr = pentr[0]
            if pentr.stat == KW.STAT['R'].id:
                  # Disallow editing rejected entries since there is seldom
                  # a need to and when previously allowed it led to abuse.
                  # The web interface does not provide an Edit button for
                  # rejected entries but a submission could be constructed
                  # to do so.
                  #FIXME? allow editors to edit?
                errs.append ("Rejected entries can not be edited.")
                return err

              # Augment the xrefs so when hist diffs are generated, they
              # will show xref details.
            jdb.augment_xrefs (dbh, raw['xref'])
            jdb.augment_xrefs (dbh, raw['xrer'], rev=True)

            if entr.stat == KW.STAT['D'].id or disp == 'r':
                  # If this is a deletion or rejection, set 'use_parent'.
                  # It will be passed to function jdb.add_hist() and tell
                  # it to return the parent entry rather than the entry we
                  # received.  The reason is that if we are doing a delete
                  # or reject, we do not want to make any changes to the
                  # entry, even if the submitter has done so.
                use_parent = True

        if entr.stat==KW.STAT['D'].id and not getattr (entr, 'dfrm', None):
            L(lgid).debug("delete of new entry error")
            errs.append ("Delete requested but this is a new entry.")
        if disp == 'a':     # Can only approve the head entry.
            if len(edchain)>1 and entr.dfrm != edchain[-1]:
                L(lgid).debug("approve non-head entry error")
                errs.append ("You can only approve the latest edit")
        if disp == 'a' and has_xrslv (entr) and entr.stat==KW.STAT['A'].id:
            L(lgid).debug("unresolved xrefs error")
            errs.append ("Can't approve because entry has unresolved xrefs.")
        if errs: return err

        if not use_parent:
              # Check the entry for errors (like missing reading, etc).
              # only if not using the parent entry for the submission.
              # If we are, check is unnecessary; we don't care what garbage
              # the user submitted since the parent will be used instead.
            check_entr (entr, errs)
            if errs: return err

          # If this is a submission by a non-editor, restore the
          # original entry's freq items which non-editors are not
          # allowed to change.
        if not is_editor:
            if pentr:
                L(lgid).debug("copying freqs from parent")
                jdb.copy_freqs (pentr, entr)
              # Note that non-editors can provide freq items on new
              # entries.  We expect an editor to vet this when approving.

          # Entr contains the hist record generated by the caller.  Extract
          # the user-discretionary info from it (name, email, notes, refs)
          # and create a new trusted history record with non-discretionary
          # parts supplied by us.  If no history record was provided (common
          # with test submissions, etc.), create a new empty one.
        h = entr._hist[-1] if entr._hist else jdb.Hist()
          # When we get here, if 'use_parent' is true, pentr will also
          # be true.  If we are wrong, add_hist() will throw an exception
          # but will never return a None, so no need to check return val.
        L(lgid).debug("adding hist for '%s', merge=%s"
                                           % (h.name, use_parent))
          # Check that the user-supplied info has no ascii control
          # characters.  If there are, the submission is not done
          # rather than quietly doing the cleaning ourselves.
          # Justification is that the caller (in the cgi case that
          # would be the edconf.py page) should have already checked
          # and fixed the problem.
        entr = jdb.add_hist (entr, pentr, userid,
                             clean (h.name, 'submitter name', errs),
                             clean (h.email, 'submitter emil', errs),
                             clean (h.notes, 'comments', errs),
                             clean (h.refs, 'refs', errs),
                             use_parent)
        if errs: return err

          # Occasionally, often from copy-pasting, a unicode BOM
          # character finds its way into one of an entry's text
          #  strings.  We quietly remove any here.
        n = jdb.bom_fixall (entr)
        if n > 0:
            L(lgid).debug("removed %s BOM character(s)" % n)

          # The returned 'entr' object is the parameter 'entr' object
          # if 'use_parent' is false or the parent of the parameter 'entr'
          # if 'use_parent' is true.
        return entr, pentr, edchain, opid, root_unap

  # Used several places in the code.
Noentr_msg = "The entry you are editing no longer exists, "\
      "likely because it was approved, deleted or rejected "\
      "before your changes were submitted.  "\
      "Please search for the current version of the entry and "\
      "reenter your changes if they are still applicable."

def check_entr (e, errs):
        # Do some validation of the entry.  This is nowhere near complete.
        # The database integrity rules will in principle catch all serious
        # problems but catching db errors and back translating them to a
        # user-actionable message is difficult so we try to catch the obvious
        # stuff here.

        if not e.src:
            errs.append ("No Corpus value selected.  Please select the "
                         "corpus that this entry will be added to.")
        if not e._rdng and src_type (e.src) == 'jmdict':
            errs.append ("No readings were entered for this entry.  "\
                         "All JMdict entries require a reading.")
        if not e._rdng and not e._kanj:
            errs.append ("Both the Kanji and Reading boxes are empty.  "
                         "You must provide at least one of them.")
        if not e._sens:
            msg = "No senses given.  You must provide at least one sense."
            errs.append (msg)
        for n, s in enumerate (e._sens):
            if not s._gloss:
                msg = "Sense %d has no glosses.  Every sense must have "\
                      "at least one regular gloss, or a [lit=...] or "\
                      "[expl=...] tag." % (n+1)
                errs.append (msg)
            if not s._pos and src_type (e.src) == 'jmdict':
               errs.append ("Sense %d has no PoS (part-of-speech) tag.  "\
                            "Every sense must have at least one." % (n+1))

          # Check for duplicate reading, kanji or gloss text (IS-205).
        nodups, dups = jdb.rmdups (e._rdng, lambda x: x.txt)
        if dups: errs.append ("Duplicate readings were given."
                              "  Please remove the extra readings: %s"
                              % ", ".join (x.txt for x in dups))
        nodups, dups = jdb.rmdups (e._kanj, lambda x: x.txt)
        if dups: errs.append ("Duplicate kanji were given."
                              "  Please remove the extra kanji: %s"
                              % ", ".join (x.txt for x in dups))
        for n, s in enumerate (e._sens):
              # Note that duplicate glosses are per sense and per langauge;
              # duplicates with different languages are ok.
            nodups, dups = jdb.rmdups (s._gloss, lambda x: (x.lang,x.txt))
            if dups: errs.append ("Duplicate gloss were given in sense %d."
                              "  Please remove the extra gloss: %s"
                              % (n+1, ", ".join (x.txt for x in dups)))

def _submit (dbh, entr):
        """===================================================================
        Submit an unapproved entry.
        Write entry object 'entr' to the database as an "active" or
        "deleted", unapproved (entr.unap=True) entry.  'entr' must have
        a .dfrm attribute value with the id number of the entry in the
        database that it is an edit of, or be None (it is a new entry).
        Returns: 3-tuple of the new entry's id number, sequence number
          and corpus (aka"src") number.
        ===================================================================="""
        KW = jdb.KW
        L('submit._submit').debug("entr.dfrm: %r" % entr.dfrm)
          # Can't submit a "reject" entry, use _reject() instead.
        assert entr.stat != jdb.KW.STAT['R'].id
          # Only "active" entries can be submitted without a parent (ie
          # be a new entry); "delete" entries must have parent.
        assert entr.dfrm or entr.stat == KW.STAT['A'].id
        assert entr.unap
        r = _addentr (dbh, entr)
        L('submit._submit').debug("added entry: %r" % ((r,)))
        return r        # r is 3-tuple: (id#, seq#, src#)

def _approve (dbh, entr, root):
        """===================================================================
        Remove all entries on the edit chain starting at 'root' (which
        should be the root of its edit chain)and replace with approved
        entry 'entr'.
        Returns: 3-tuple of the new entry's id number, sequence number
          and corpus (aka"src") number.
        ==================================================================="""
        KW = jdb.KW
        L('submit.approve').debug("approving entr id %s" % entr.dfrm)
          # stat may be A or D, but not R.
        assert entr.stat != KW.STAT['R'].id
        entr.unap, entr.dfrm = False, None
          # Delete the old root if any.  We need to delete the old active
          # entry before adding the new one, to avoid triggering the
          # database constraint the prohibits two active, approved entries.
          # Because the dfrm foreign key is "on delete cascade", deleting
          # the root entry will also delete all it's children.
        if root: _delentr (dbh, root)
          # With the old approved entry gone we can write the new one to
          # the database.
        r = _addentr (dbh, entr)
        L('submit._approve').debug("added approved entry: %r" % ((r,)))
        return r        # r is 3-tuple: (id#, seq#, src#)

def _reject (dbh, entr, rej_from):
        """===================================================================
        Remove all entries from 'rej_from' to its head entry and replace
        with rejected entry 'entr'.
        Returns: 3-tuple of the new entry's id number, sequence number
          and corpus (aka src) number.
        ==================================================================="""
        KW = jdb.KW
          # Prepare the "reject" entry.
        entr.stat = KW.STAT['R'].id
        entr.unap, entr.dfrm = False, None
          # Delete entry id# 'rej_from'.   When we delete it, all the
          # following entries up to and including the head (most recently
          # edited) entry will also be deleted due to the CASCADE qualifier
          # on the entr.dfrm column in the database.
        L('submit._reject').debug("rej_from: %r" % rej_from)
        _delentr (dbh, rej_from)
          # Now write the new rejected entry to the database.
        L('submit.reject').debug("adding rejected entry")
        r = _addentr (dbh, entr)
        L('submit.reject').debug("added rejected entry: %r" % ((r,)))
        return r        # r is 3-tuple: (id#, seq#, src#)

def _addentr (dbh, entr):
        entr._hist[-1].unap = entr.unap
        entr._hist[-1].stat = entr.stat
        L('submit._addentr').debug("adding entry to database")
        L('submit._addentr').debug("%d hists, last hist is %s [%s] %s"
          % (len(entr._hist), entr._hist[-1].dt, entr._hist[-1].userid,
             entr._hist[-1].name))
        r = jdb.addentr (dbh, entr)
        msg = "entry id=%s, seq=%s, src=%s added to database" % r
        L('submit._addentr').info(msg)
        return r        # r is 3-tuple: (id#, seq#, src#)

def _delentr (dbh, id):
        # Delete entry 'id' (and by cascade, any edited entries
        # based on this one).  This function deletes the entire
        # entry, including history.  To delete the entry contents
        # but leaving the entr and hist records, use database
        # function delentr.  'dbh' is an open dbapi cursor object.

        L('submit._delentr').debug("deleting entry id %s from database"%id)
        sql = "DELETE FROM entr WHERE id=%s";
        dbh.execute (sql, (id,))

def has_xrslv (entr):
        for s in entr._sens:
            if getattr (s, '_xrslv', None): return True
        return False

def get_edchain (dbh, eid):
        """===================================================================
        #  Versions of entries are linked together in a chain of edits via
        #  each entry's .dfrm field, which references the parent entr from
        #  which the referencing entry was derived.  A database unique
        #  constraint on entr.dfrm enforces the condition that any parent
        #  entry can have no more than one child entry.
        #  This function returns 2-tuple of:
        #    - A list of entry id#s from the root entry (entry whose .dfrm
        #      field is null; the "oldest" entry in the chain) to the head
        #      entry (entry to which no other entry's .dfrm fields point;
        #      the most recently edited entry) of the edit chain in which
        #      'eid' occurs somewhere.
        #    - A bool: True is the root entry is unapproved, False if it
        #      is approved.
        #  If an entry has no edits (there is no other entry with its .dfrm
        #  pointing to it) and is not an edit of some other entry (its own
        #  .dfrm value is null), the first item of the return tuple is a
        #  list of a single item: that entry's id#.  If an entry with id#
        #  'eid' doesn't exist, the 2-tuple (None,None) is returned.
        #
        #  Example:  Given the edit chain (A,B,C represent entries; "<--"
        #  represent .dfrm links; A#,B#,C# represent id#s of respective
        #  entries; "*" represents an unapproved entry):
        #    A <-- B* <-- C*
        #  then any of:
        #     get_edchain (dbh, A#)
        #     get_edchain (dbh, B#)
        #     get_edchain (dbh, C#)
        #  will return:
        #     ([A#, B#, C#], False)
        #  See also: db/mkviews.sql view "edchains".
        ===================================================================="""
        if eid is None: raise ValueError (eid)
        sql = "SELECT id,root,head,path FROM edchain WHERE id=%s"
        rs = db.query (dbh.connection, sql, (eid,))
        if len(rs) == 0: return None,None       # Entry 'eid' does not exist.
        assert len(rs) == 1     # Impossible to get mult. rows for one 'eid'.
        _, rootid, _, chain = rs[0]
          # If no 'rootid' (implies no 'chain' as well) then 'eid' is a lone
          # entry not part of any edit chain.
        if not rootid: rootid, chain = eid, [eid]
          # Get the full entr record for the root entry.
        sql = "SELECT * FROM entr WHERE id=%s"
        entr = db.query1 (dbh.connection, sql, (rootid,))
          # It not uncommon for the target entry to disappear between the
          # time this function was called and we get here -- an edited entry
          # could have been approved by someone else for example.
        if not entr: return None,None
          # The reject() function needs to know if the root entry is
          # approved or not.
        root_unap = entr[5]
        return chain, root_unap

def logseq (cur, seq, src):
        """===================================================================
        # Return a list of the id,dfrm pairs (plus stat and unap) for
        # all entries in a src/seq set.  We format the list into a text
        # string for the caller to print.  By looking at this someome
        # can figure out the shape of the edit tree (assuming that it
        # is entirely in the src/seq set).
        ==================================================================="""
        sql = "SELECT id,dfrm,stat,unap FROM entr"\
              " WHERE seq=%s AND src=%s ORDER by id"
        cur.execute (sql, (seq,src))
        rs = cur.fetchall()
        return ','.join ([str(r) for r in rs])

def url (entrid):
        return '<a href="entr.py?svc=%s&sid=%s&e=%s">%s</a>' \
                 % (Svc, Sid, entrid, entrid)

def src_type (src):
        '  Return the src type keyword for a src id.'
        if not src: return ''
        return jdb.KW.SRCT[jdb.KW.SRC[src].srct].kw

def clean (s, source=None, errs=None):
        '''-------------------------------------------------------------------
        Remove all ascii control characters except '\n's from str 's'.
        If 'source' is not None it indicates an error message will
        be appended to list 'errs' (which should also be supplied)
        if any characters are removed and is a str giving the source
        of 's' for use in the error message (eg. "history comments",
        "gloss", etc.)
        -------------------------------------------------------------------'''
        if not s: return s
        N = None
          # Delete all ascii control characters in 's' except for
          # '\n' ('\x0a') and tabs ('\x09') which are expanded.
          #FIXME: what about unicode bogons?
        cleaned = s.translate ([
          #  00 01 02 03 04 05 06 07 08  09    0a     0b 0c 0d 0e 0f
             N, N, N, N, N, N, N, N, N, '\x09','\x0a',N, N, N, N, N,
          #  10 11 12 13 14 15 16 17 18  19    1a     1b 1c 1d 1e 1f
             N, N, N, N, N, N, N, N, N,  N,    N,     N, N, N, N, N ])
          # Expand any tabs to spaces.
          # [Postponing the tab expansion, needs more consideration
          # and discussion.]
        #cleaned = cleaned.expandtabs()
        if source and cleaned != s:
            errs.append ("Illegal characters in '%s'" % source)
        return cleaned
