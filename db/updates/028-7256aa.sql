\set ON_ERROR_STOP
BEGIN;

-- Add new kwmisc values "net-sl", "dated", "hist", "lit".
-- Change kwpos value "cop-da" to "cop".

  -- Jmdictdb schema version id(s) to update database to and current
  -- schema version id(s) required for this update to be applied.
\set dbversion  '''7256aa'''
\set require    '''12b5e2'''

\qecho Checking database version...
SELECT CASE WHEN (EXISTS (SELECT 1 FROM db WHERE id=x:require::INT)) THEN NULL
    ELSE (SELECT err('Database at wrong update level, need version '||:require)) END;
INSERT INTO db(id) VALUES(x:dbversion::INT);
UPDATE db SET active=FALSE WHERE id!=x:dbversion::INT;


-- Do the update

INSERT INTO kwmisc VALUES(85,'net-sl','Internet slang');
INSERT INTO kwmisc VALUES(86,'dated','dated term');
INSERT INTO kwmisc VALUES(87,'hist','historical term');
INSERT INTO kwmisc VALUES(88,'litf','literary or formal term');
UPDATE kwpos SET kw='cop' WHERE id=15;

COMMIT;
