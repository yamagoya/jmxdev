\set ON_ERROR_STOP
BEGIN;

-- Enforce linear non-branching edits at database level
--
-- This creates a unique constraint on the entr.dfrm column which
-- will prevent more than one edit child per parent entry.  This
-- prevents the edit trees supported in earlier revs and enforces
-- linear chains of edits instead.
-- While the previous views, "edbase", "edpath" and "edpaths" were
-- usable with non-branching edits, they are overly complex for
-- finding edit chains and have been replaced by view "edchains".

  -- Jmdictdb schema version id(s) to update database to and current
  -- schema version id(s) required for this update to be applied.
\set dbversion  '''6f3d81'''
\set require    '''7bb062'''

\qecho Checking database version, 0 rows expected...
SELECT vchk (:require);                      -- Will raise error on failure.
INSERT INTO db(id) VALUES(x:dbversion::INT); -- Make this version active.
-- This update supercedes previous updates.
UPDATE db SET active=FALSE WHERE active AND  -- Deactivate all :require.
  LPAD(TO_HEX(id),6,'0') IN (SELECT unnest(string_to_array(:require,',')));


-- Do the update.

DROP INDEX IF EXISTS entr_dfrm_idx;
CREATE UNIQUE INDEX ON entr(dfrm) WHERE dfrm IS NOT NULL;
-- To reverse the above change:
-- DROP INDEX IF EXISTS entr_dfrm_idx;
-- CREATE INDEX ON entr(dfrm) WHERE dfrm IS NOT NULL;

DROP VIEW IF EXISTS edbase, edpath, edpaths, edroot;

-- See db/mkviews.sql for a description of these views:

CREATE OR REPLACE VIEW edchains AS (   -- For linear edits,
    WITH RECURSIVE w(id,head,dfrm,path) AS (
        SELECT e.id,e.id,e.dfrm,ARRAY[e.id]
            FROM entr e     -- These are head entries.
            WHERE e.dfrm is NOT NULL AND NOT EXISTS
              (SELECT 1 FROM entr e2 WHERE e2.dfrm=e.id)
        UNION ALL
        SELECT e.id,w.head,e.dfrm,e.id||path
            FROM w JOIN entr e ON w.dfrm=e.id)
    SELECT id AS root,head,path FROM w WHERE dfrm IS NULL);

CREATE OR REPLACE VIEW edchain AS (
    SELECT e.id,c.*
    FROM entr e LEFT JOIN edchains c ON e.id=ANY(c.path));

COMMIT;
