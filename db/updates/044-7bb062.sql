\set ON_ERROR_STOP
BEGIN;

-- Bring an updated schema into conformity with a newly created one.
--
-- Consecutive updates applied to a preexisting JMdictDB database are
-- intended to result in the same schema as a newly created database
-- but over time mistakes happen and there are currently identifiable
-- discrepencies between a database created many years ago and updated
-- over time and a newly created one.
-- The following changes were developed by comparing the schema of a
-- newly created database to that of a very old one that has had all
-- the update scripts applied.
-- This script is idempotent and can be applied to a database without
-- some or any of differences to be corrected with no ill effect.

  -- Jmdictdb schema version id(s) to update database to and current
  -- schema version id(s) required for this update to be applied.
\set dbversion  '''7bb062'''
\set require    '''2a6bd1'''

\qecho Checking database version, 0 rows expected...
SELECT vchk (:require);                      -- Will raise error on failure.
INSERT INTO db(id) VALUES(x:dbversion::INT); -- Make this version active.
-- This update supercedes previous updates.
UPDATE db SET active=FALSE WHERE active AND  -- Deactivate all :require.
LPAD(TO_HEX(id),6,'0') IN (SELECT unnest(string_to_array(:require,',')));


-- Do the update.

  -- The "edroot" view added in db-2a6db1 (230420-b93e6ae) was totally
  -- bogus.  This is a corrected version. See the comments in mkviews.sql
  -- for full description.
DROP VIEW edroot;
CREATE OR REPLACE VIEW edroot AS (
    WITH RECURSIVE w(id,root) AS (
        SELECT e.id,e.id FROM entr e
            WHERE e.dfrm is NULL AND EXISTS
              (SELECT 1 FROM entr e2 WHERE e2.dfrm=e.id)
        UNION ALL
        SELECT e.id,w.root
            FROM w JOIN entr e ON w.id=e.dfrm)
    SELECT id,root FROM w);

  -- Should have dropped these views when other vt_* views that are
  -- now defined in supl-views.sql were dropped (old/013.sql?)
DROP VIEW IF EXISTS vt_gloss, vt_gloss2, vt_kanj, vt_kanj2, vt_kinf,
  vt_misc, vt_pos, vt_rdng, vt_rdng2, vt_rinf, vt_sens, vt_sens2, vt_sens3;

  -- These have never found any use; moved to supl-views.sql.
DROP VIEW IF EXISTS essum, ssum;

  -- Never used and outdated functions.
DROP FUNCTION IF EXISTS dupentr(INT), delentr(INT);

  -- "kresolv" replaced by table krslv in 035-8fac2c.sql (200819-6426810)
  -- but neglected to drop the original table.
DROP TABLE IF EXISTS kresolv;

  -- Views hdwds and is_p used "e.*" in their select lists which creates
  -- a discrepancy between views created after column "idx" was added to
  -- table entr: newly created views will contain that column but views
  -- created before it was added won't (since no subsequent updates ever
  -- recreated those views).  We recreate them now with an explicit
  -- column list without "idx" (since nothing currently expects it to
  -- be present.)  The definitions in mkviews.sql were also changed to
  -- match these.  esum is also recreated since it is dependent on is_p.
DROP VIEW IF EXISTS is_p, hdwds, esum;

CREATE OR REPLACE VIEW hdwds AS (
    SELECT e.id,e.src,e.stat,e.seq,e.dfrm,e.unap,e.srcnote,e.notes,
        r.txt AS rtxt,k.txt AS ktxt
    FROM entr e
    LEFT JOIN rdng r ON r.entr=e.id
    LEFT JOIN kanj k ON k.entr=e.id
    WHERE (r.rdng=1 OR r.rdng IS NULL)
      AND (k.kanj=1 OR k.kanj IS NULL));

CREATE OR REPLACE VIEW is_p AS (
    SELECT e.id,e.src,e.stat,e.seq,e.dfrm,e.unap,e.srcnote,e.notes,
        EXISTS (
            SELECT * FROM freq f
            WHERE f.entr=e.id
              -- ichi1, gai1, news1, or specX
              AND ((f.kw IN (1,2,7) AND f.value=1)
                OR f.kw=4)) AS p
    FROM entr e);

CREATE OR REPLACE VIEW esum AS (
    SELECT e.id,e.seq,e.stat,e.src,e.dfrm,e.unap,e.notes,e.srcnote,
        h.rtxt AS rdng,
        h.ktxt AS kanj,
        (SELECT ARRAY_TO_STRING(ARRAY_AGG( ss.gtxt ), ' / ')
         FROM
            (SELECT
                (SELECT ARRAY_TO_STRING(ARRAY_AGG(sg.txt), '; ')
                FROM (
                    SELECT g.txt
                    FROM gloss g
                    WHERE g.sens=s.sens AND g.entr=s.entr
                    ORDER BY g.gloss) AS sg
                ORDER BY entr,sens) AS gtxt
            FROM sens s WHERE s.entr=e.id ORDER BY s.sens) AS ss) AS gloss,
        (SELECT COUNT(*) FROM sens WHERE sens.entr=e.id) AS nsens,
        (SELECT p FROM is_p WHERE is_p.id=e.id) AS p,
        (NOT EXISTS (SELECT 1 FROM entr child WHERE child.dfrm=e.id)) AS leaf
    FROM entr e
    JOIN hdwds h on h.id=e.id);

  -- Reverse the order of ord,typ in xresolv primary key.
ALTER TABLE xresolv
  DROP CONSTRAINT xresolv_pkey,         -- (entr, sens, ord, typ)
  ADD  CONSTRAINT xresolv_pkey PRIMARY KEY (entr, sens, typ, ord);

  -- Early versions of the JMdictDB schema separated the table and fkey
  -- definitions because the latter had to be added and removed independently
  -- of the tables.  And that required assigning explict fkey names which
  -- were sometimes chosen to be other than the names that Postgresql would
  -- have been assigned by default.  Later versions of the schema included
  -- the fkey definitions with the tables and allowed postgresql to assign
  -- default names.  The updates below change the earlier explicit names to
  -- the default ones.  The names need to be consistent across databases to
  -- allow writing update scripts that change the fkeys.
  -- The "ALTER TABLE ... RENAME CONSTRAINT" statment has no provision
  -- for not raising an error if the constraint does not exist.  So
  -- we create a function that will check for the fkey's existance before
  -- trying to rename it.

CREATE FUNCTION rename_fkey (text, text, text)
    -- Renames a foreign key constraint without an error if not exists.
    -- Parameters: tablename, oldname (fkey), newname (fkey)
      RETURNS setof VOID AS $$
    DECLARE
      sql TEXT := 'ALTER TABLE '||$1||' RENAME CONSTRAINT '||$2||' TO '||$3;
    BEGIN
      IF EXISTS
        (SELECT 1 FROM information_schema.constraint_column_usage
         WHERE table_schema='public' AND constraint_name=$2)
      THEN execute sql;
      ELSE RAISE NOTICE 'Constraint "%" not found', $2;
      END IF;
    END $$ language 'plpgsql';

\qecho Renaming foreign keys...
\out /dev/null
SELECT rename_fkey ('conjo_notes', 'conjo_notes_pos_fkey', 'conjo_notes_pos_conj_neg_fml_onum_fkey');
SELECT rename_fkey ('dial', 'dial_entr_fkey', 'dial_entr_sens_fkey');
SELECT rename_fkey ('fld', 'fld_entr_fkey', 'fld_entr_sens_fkey');
SELECT rename_fkey ('freq', 'freq_entr_fkey', 'freq_entr_rdng_fkey');
SELECT rename_fkey ('freq', 'freq_entr_fkey1', 'freq_entr_kanj_fkey');
SELECT rename_fkey ('gloss', 'gloss_entr_fkey', 'gloss_entr_sens_fkey');
SELECT rename_fkey ('kinf', 'kinf_entr_fkey', 'kinf_entr_kanj_fkey');
SELECT rename_fkey ('lsrc', 'lsrc_entr_fkey', 'lsrc_entr_sens_fkey');
SELECT rename_fkey ('misc', 'misc_entr_fkey', 'misc_entr_sens_fkey');
SELECT rename_fkey ('pos', 'pos_entr_fkey', 'pos_entr_sens_fkey');
SELECT rename_fkey ('rdngsnd', 'rdngsnd_entr_fkey', 'rdngsnd_entr_rdng_fkey');
SELECT rename_fkey ('restr', 'restr_entr_fkey', 'restr_entr_rdng_fkey');
SELECT rename_fkey ('restr', 'restr_entr_fkey1', 'restr_entr_kanj_fkey');
SELECT rename_fkey ('rinf', 'rinf_entr_fkey', 'rinf_entr_rdng_fkey');
SELECT rename_fkey ('stagk', 'stagk_entr_fkey', 'stagk_entr_sens_fkey');
SELECT rename_fkey ('stagk', 'stagk_entr_fkey1', 'stagk_entr_kanj_fkey');
SELECT rename_fkey ('stagr', 'stagr_entr_fkey', 'stagr_entr_sens_fkey');
SELECT rename_fkey ('stagr', 'stagr_entr_fkey1', 'stagr_entr_rdng_fkey');
SELECT rename_fkey ('xref', 'xref_entr_fkey', 'xref_entr_sens_fkey');
SELECT rename_fkey ('xref', 'xref_xentr_fkey', 'xref_xentr_xsens_fkey');
SELECT rename_fkey ('xresolv', 'xresolv_entr_fkey', 'xresolv_entr_sens_fkey');
\out
\qecho ...done renaming foreign keys

DROP FUNCTION rename_fkey (text, text, text);

-- Recreate view dbx for the benefit of the jmtest01 database which
-- somehow missed the update that added column "idd".
DROP VIEW IF EXISTS dbx;
CREATE OR REPLACE VIEW dbx AS (
    SELECT LPAD(TO_HEX(id),6,'0') AS id, active, ts, id AS idd
    FROM db
    ORDER BY ts DESC);

\qecho Any "NOTICE" messages that occur above are ok and indicate that some
\qecho of the corrections this update applies are already present.

COMMIT;
