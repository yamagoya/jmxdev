#!/bin/sh
# This script will set up two Git hooks that will execute after
# each JMdictDB checkout or commit and will update the jmdict/-
# __version__.py file to the current git revision number.
# It should be run from the jmdictdb root directory and need be
# run only once after cloning a jmdictdb repo.
# The __version__.py contents are used in the jmdictdb package
# name created when installing the software and are also displayed
# by the cgiinfo.py web page; these hooks help keep it in sync with
# the actual code being run.  Note that the Makefile script updates
# the version file when installing the JMdictDB package so the hooks
# aren't strictly necessary when installing the jmdictdb package
# but are important when running code directly from the development
# directory during development.

set -e
cat >.git/hooks/post-commit <<EOD
#!/bin/sh
tools/upd-version.py
EOD
chmod 755 .git/hooks/post-commit
( cd .git/hooks && cp -a post-commit post-checkout )
echo "post-checkout and post-commit hooks installed"
